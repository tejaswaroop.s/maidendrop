<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');
include 'Excel/Classes/PHPExcel.php';

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

if ($_SESSION['userdetails']->userid == 1) :

    function checkApplicationNumberExists($applicationNumber)
    {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $sql = "SELECT * from users where applicationnumber = '{$applicationNumber}'";
        $query = $dbh->prepare($sql);
        $query->execute();
        $applications = $query->fetchAll(PDO::FETCH_OBJ);
        if (count($applications) == 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_useridby_employeeid($employeeid)
    {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $sql = "SELECT * from users where employeeid = '{$employeeid}'";
        $query = $dbh->prepare($sql);
        $query->execute();
        $applications = $query->fetchAll(PDO::FETCH_OBJ);
        if (count($applications) > 0) {
            return $applications[0]->userid;
        }
    }

    function get_useridby_applicationnumber($applicationnumber)
    {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $sql = "SELECT * from users where applicationnumber = '{$applicationnumber}'";
        $query = $dbh->prepare($sql);
        $query->execute();
        $applications = $query->fetchAll(PDO::FETCH_OBJ);
        if (count($applications) > 0) {
            return $applications[0]->userid;
        }
    }

    function get_batch()
    {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $sql = "SELECT * FROM batch where isactive = 1";
        $query = $dbh->prepare($sql);
        $query->execute();
        $batch = $query->fetchAll(PDO::FETCH_OBJ);
        if (count($batch) > 0) {
            return $batch[0]->year;
        }
    }

    function get_paymentidcounter()
    {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $sql = "SELECT * FROM paymentidcounter";
        $query = $dbh->prepare($sql);
        $query->execute();
        $paymentidcounter = $query->fetchAll(PDO::FETCH_OBJ);
        if (count($paymentidcounter) > 0) {
            return $paymentidcounter[0]->nextpaymentid;
        }
    }

    function set_paymentidcounter()
    {
        $current = get_paymentidcounter();

        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $nextpaymentid = $current + 1;

        $sql = "UPDATE paymentidcounter SET nextpaymentid = {$nextpaymentid}";
        $query = $dbh->prepare($sql);
        $query->execute();
    }

    if (isset($_FILES['employees_upload'])) {
        $tmpfname = $_FILES['employees_upload']['tmp_name'];
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $errorCode = "";
        $successCode = "";

        for ($row = 2; $row <= $lastRow; $row++) {

            $employeeid = $worksheet->getCell('A' . $row)->getValue();
            $name = $worksheet->getCell('B' . $row)->getValue();
            $designation = $worksheet->getCell('C' . $row)->getValue();
            $branchid = $worksheet->getCell('D' . $row)->getValue();
            $mobile1 = $worksheet->getCell('E' . $row)->getValue();
            $email = $worksheet->getCell('F' . $row)->getValue();
            $door_street = $worksheet->getCell('G' . $row)->getValue();
            $mobile2 = $worksheet->getCell('H' . $row)->getValue();
            $genderid = $worksheet->getCell('I' . $row)->getValue();
            $dateofbirth = $worksheet->getCell('J' . $row)->getValue();
            $bloodgroup = $worksheet->getCell('K' . $row)->getValue();

            $dobArray = explode('--', $dateofbirth);
            $dob = "{$dobArray[2]}-{$dobArray[1]}-{$dobArray[0]}";

            $username = $email;
            $password = md5($mobile1);
            $roleid = 7;

            $sql = "INSERT INTO users (bloodgroup,designation, employeeid, username, password, roleid, name, dateofbirth, genderid, branchid, door_street, mobile1, mobile2, email) 
                              VALUES (:bloodgroup,:designation,:employeeid,:username,:password,:roleid,:name,:dateofbirth,:genderid,:branchid,:door_street,:mobile1,:mobile2,:email)";

            $query = $dbh->prepare($sql);
            $query->bindParam(':bloodgroup', $bloodgroup, PDO::PARAM_STR);
            $query->bindParam(':designation', $designation, PDO::PARAM_STR);
            $query->bindParam(':employeeid', $employeeid, PDO::PARAM_STR);
            $query->bindParam(':username', $username, PDO::PARAM_STR);
            $query->bindParam(':password', $password, PDO::PARAM_STR);
            $query->bindParam(':roleid', $roleid, PDO::PARAM_STR);
            $query->bindParam(':name', $name, PDO::PARAM_STR);
            $query->bindParam(':dateofbirth', $dob, PDO::PARAM_STR);
            $query->bindParam(':genderid', $genderid, PDO::PARAM_STR);
            $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
            $query->bindParam(':door_street', $door_street, PDO::PARAM_STR);
            $query->bindParam(':mobile1', $mobile1, PDO::PARAM_STR);
            $query->bindParam(':mobile2', $mobile2, PDO::PARAM_STR);
            $query->bindParam(':email', $email, PDO::PARAM_STR);

            $issuccess = $query->execute();
        }
    }

    if (isset($_FILES['students_upload'])) {
        $tmpfname = $_FILES['students_upload']['tmp_name'];
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $errorCode = "";
        $successCode = "";

        for ($row = 2; $row <= $lastRow; $row++) {

            $applicationnumber = $worksheet->getCell('A' . $row)->getValue();
            if (checkApplicationNumberExists($applicationnumber)) {
                $admissiontypeid = $worksheet->getCell('B' . $row)->getValue();
                $branchid = $worksheet->getCell('C' . $row)->getValue();
                $courseid = $worksheet->getCell('D' . $row)->getValue();
                $termid = $worksheet->getCell('E' . $row)->getValue();
                $secondlanguageid = $worksheet->getCell('F' . $row)->getValue();
                $coachingfee = $worksheet->getCell('G' . $row)->getValue();
                $name = $worksheet->getCell('H' . $row)->getValue();
                $dateofbirth = $worksheet->getCell('I' . $row)->getValue();

                if ($dateofbirth != "") {
                    $dobArray = explode('--', $dateofbirth);
                    $dob = "{$dobArray[2]}-{$dobArray[1]}-{$dobArray[0]}";
                }

                $genderid = $worksheet->getCell('J' . $row)->getValue();
                $nationalityid = $worksheet->getCell('K' . $row)->getValue();
                $religionid = $worksheet->getCell('L' . $row)->getValue();
                $categoryid = $worksheet->getCell('M' . $row)->getValue();
                $studentaadhaar = $worksheet->getCell('N' . $row)->getValue();
                $fathername = $worksheet->getCell('O' . $row)->getValue();
                $mothername = $worksheet->getCell('P' . $row)->getValue();
                $parentoccupation = $worksheet->getCell('Q' . $row)->getValue();
                $boardid = $worksheet->getCell('R' . $row)->getValue();
                $schoolname = $worksheet->getCell('S' . $row)->getValue();
                $grade = $worksheet->getCell('T' . $row)->getValue();
                $hallticket = $worksheet->getCell('U' . $row)->getValue();
                $placeofeducation = $worksheet->getCell('V' . $row)->getValue();
                $door_street = $worksheet->getCell('W' . $row)->getValue();
                $village_mandal = $worksheet->getCell('X' . $row)->getValue();
                $landmark = $worksheet->getCell('Y' . $row)->getValue();
                $city_town = $worksheet->getCell('Z' . $row)->getValue();
                $district = $worksheet->getCell('AA' . $row)->getValue();
                $pin = $worksheet->getCell('AB' . $row)->getValue();
                $mobile1 = $worksheet->getCell('AC' . $row)->getValue();
                $mobile2 = $worksheet->getCell('AD' . $row)->getValue();
                $email = $worksheet->getCell('AE' . $row)->getValue();

                $referer = $worksheet->getCell('AF' . $row)->getValue();
                $referredby = get_useridby_employeeid($referer);
                $admissiondate = $worksheet->getCell('AG' . $row)->getValue();
                if ($admissiondate != "") {
                    $admissionArray = explode('--', $admissiondate);
                    $admdate = "{$admissionArray[2]}-{$admissionArray[1]}-{$admissionArray[0]}";
                }

                $now = new DateTime();
                $username = "USR-" . $now->format('dmYHisu');
                $password = md5($username);
                $roleid = 5;

                $applicationstatusid = 4;
                $sendapprovalto = 'SA';

                $sql = "INSERT INTO users (admissiondate, username, password, roleid, name, fathername, mothername, parentoccupation, studentaadhaar, boardid, hallticket, dateofbirth, nationalityid, religionid, categoryid, schoolname, grade, placeofeducation, genderid, admissiontypeid, branchid, courseid, termid, secondlanguageid, door_street, village_mandal, landmark, city_town, district, pin, mobile1, mobile2, email, applicationstatusid, applicationnumber, sendapprovalto, coachingfee, referredby) 
                                  VALUES (:admissiondate,:username,:password,:roleid,:name,:fathername,:mothername,:parentoccupation,:studentaadhaar,:boardid,:hallticket,:dateofbirth,:nationalityid,:religionid,:categoryid,:schoolname,:grade,:placeofeducation,:genderid,:admissiontypeid,:branchid,:courseid,:termid,:secondlanguageid,:door_street,:village_mandal,:landmark,:city_town,:district,:pin,:mobile1,:mobile2,:email,:applicationstatusid,:applicationnumber,:sendapprovalto,:coachingfee,:referredby)";

                $query = $dbh->prepare($sql);
                $query->bindParam(':admissiondate', $admdate, PDO::PARAM_STR);
                $query->bindParam(':username', $username, PDO::PARAM_STR);
                $query->bindParam(':password', $password, PDO::PARAM_STR);
                $query->bindParam(':roleid', $roleid, PDO::PARAM_STR);
                $query->bindParam(':name', $name, PDO::PARAM_STR);
                $query->bindParam(':fathername', $fathername, PDO::PARAM_STR);
                $query->bindParam(':mothername', $mothername, PDO::PARAM_STR);
                $query->bindParam(':parentoccupation', $parentoccupation, PDO::PARAM_STR);
                $query->bindParam(':studentaadhaar', $studentaadhaar, PDO::PARAM_STR);
                $query->bindParam(':boardid', $boardid, PDO::PARAM_STR);
                $query->bindParam(':hallticket', $hallticket, PDO::PARAM_STR);
                $query->bindParam(':dateofbirth', $dob, PDO::PARAM_STR);
                $query->bindParam(':nationalityid', $nationalityid, PDO::PARAM_STR);
                $query->bindParam(':religionid', $religionid, PDO::PARAM_STR);
                $query->bindParam(':categoryid', $categoryid, PDO::PARAM_STR);
                $query->bindParam(':schoolname', $schoolname, PDO::PARAM_STR);
                $query->bindParam(':grade', $grade, PDO::PARAM_STR);
                $query->bindParam(':placeofeducation', $placeofeducation, PDO::PARAM_STR);
                $query->bindParam(':genderid', $genderid, PDO::PARAM_STR);
                $query->bindParam(':admissiontypeid', $admissiontypeid, PDO::PARAM_STR);
                $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
                $query->bindParam(':courseid', $courseid, PDO::PARAM_STR);
                $query->bindParam(':termid', $termid, PDO::PARAM_STR);
                $query->bindParam(':secondlanguageid', $secondlanguageid, PDO::PARAM_STR);
                $query->bindParam(':door_street', $door_street, PDO::PARAM_STR);
                $query->bindParam(':village_mandal', $village_mandal, PDO::PARAM_STR);
                $query->bindParam(':landmark', $landmark, PDO::PARAM_STR);
                $query->bindParam(':city_town', $city_town, PDO::PARAM_STR);
                $query->bindParam(':district', $district, PDO::PARAM_STR);
                $query->bindParam(':pin', $pin, PDO::PARAM_STR);
                $query->bindParam(':mobile1', $mobile1, PDO::PARAM_STR);
                $query->bindParam(':mobile2', $mobile2, PDO::PARAM_STR);
                $query->bindParam(':email', $email, PDO::PARAM_STR);
                $query->bindParam(':applicationstatusid', $applicationstatusid, PDO::PARAM_STR);
                $query->bindParam(':applicationnumber', $applicationnumber, PDO::PARAM_STR);
                $query->bindParam(':sendapprovalto', $sendapprovalto, PDO::PARAM_STR);
                $query->bindParam(':coachingfee', $coachingfee, PDO::PARAM_STR);
                $query->bindParam(':referredby', $referredby, PDO::PARAM_STR);

                $issuccess = $query->execute();
                // if($issuccess != true)
                // {
                //     echo $dbh->errorInfo();
                // }

                $userid = $dbh->lastInsertId();
                if ($userid != 0) {
                    $now = new DateTime();
                    $invoiceid = "INV-" . $now->format('dmYHisu');

                    foreach ($feestructurelookup as $feestructure) {
                        if ($feestructure->admissiontypeid == $admissiontypeid) {
                            if ($feestructure->canchange == 1) {
                                $sql = "INSERT INTO invoices (invoiceid, userid, feesid, feesvalue) 
                              VALUES (:invoiceid,:userid,:feesid,:feesvalue)";

                                $query = $dbh->prepare($sql);
                                $query->bindParam(':invoiceid', $invoiceid, PDO::PARAM_STR);
                                $query->bindParam(':userid', $userid, PDO::PARAM_STR);
                                $query->bindParam(':feesid', $feestructure->feesid, PDO::PARAM_STR);
                                $query->bindParam(':feesvalue', $coachingfee, PDO::PARAM_STR);
                                $query->execute();
                            } else {
                                $sql = "INSERT INTO invoices (invoiceid, userid, feesid, feesvalue) 
                              VALUES (:invoiceid,:userid,:feesid,:feesvalue)";

                                $query = $dbh->prepare($sql);
                                $query->bindParam(':invoiceid', $invoiceid, PDO::PARAM_STR);
                                $query->bindParam(':userid', $userid, PDO::PARAM_STR);
                                $query->bindParam(':feesid', $feestructure->feesid, PDO::PARAM_STR);
                                $query->bindParam(':feesvalue', $feestructure->feesvalue, PDO::PARAM_STR);
                                $query->execute();
                            }
                        }
                    }
                }
            } else {

                // $userid = get_useridby_applicationnumber($applicationnumber);
                // $now = new DateTime();
                // $invoiceid = "INV-" . $now->format('dmYHisu');

                // $admissiontypeid = $worksheet->getCell('B' . $row)->getValue();
                // $coachingfee = $worksheet->getCell('G' . $row)->getValue();

                // foreach ($feestructurelookup as $feestructure) {
                //     if ($feestructure->admissiontypeid == $admissiontypeid) {
                //         if ($feestructure->canchange == 1) {
                //             $sql = "INSERT INTO invoices (invoiceid, userid, feesid, feesvalue) 
                //               VALUES (:invoiceid,:userid,:feesid,:feesvalue)";

                //             $query = $dbh->prepare($sql);
                //             $query->bindParam(':invoiceid', $invoiceid, PDO::PARAM_STR);
                //             $query->bindParam(':userid', $userid, PDO::PARAM_STR);
                //             $query->bindParam(':feesid', $feestructure->feesid, PDO::PARAM_STR);
                //             $query->bindParam(':feesvalue', $coachingfee, PDO::PARAM_STR);
                //             $query->execute();
                //         } else {
                //             $sql = "INSERT INTO invoices (invoiceid, userid, feesid, feesvalue) 
                //               VALUES (:invoiceid,:userid,:feesid,:feesvalue)";

                //             $query = $dbh->prepare($sql);
                //             $query->bindParam(':invoiceid', $invoiceid, PDO::PARAM_STR);
                //             $query->bindParam(':userid', $userid, PDO::PARAM_STR);
                //             $query->bindParam(':feesid', $feestructure->feesid, PDO::PARAM_STR);
                //             $query->bindParam(':feesvalue', $feestructure->feesvalue, PDO::PARAM_STR);
                //             $query->execute();
                //         }
                //     }
                // }

                $errorCode = $errorCode . "{$applicationnumber} - Duplicate Entry <br />";
            }
        }
    }

    if (isset($_FILES['payments_upload'])) {
        include 'xl/excel_reader.php';
        $tmpfname = $_FILES['payments_upload']['tmp_name'];
        $excel = new PhpExcelReader;
        $excel->read($tmpfname);

        $ExcelData = $excel->sheets[2]['cells'];
        for ($row = 2; $row <= count($ExcelData); $row++) {

            $applicationnumber = $ExcelData[$row][1];
            $userid = get_useridby_applicationnumber($applicationnumber);

            $paymenttypeid = $ExcelData[$row][36];
            $paymentamount = $ExcelData[$row][34];
            $paydate = $ExcelData[$row][35];

            if ($paydate != "") {
                $paydateArray = explode('--', $paydate);
                $paymentdate = "{$paydateArray[2]}-{$paydateArray[1]}-{$paydateArray[0]}";
            }

            $otherdetails = "Received By: {$ExcelData[$row][39]} - Other Details: " . $ExcelData[$row][37];
            $paymentcollectedby = NULL;
            $paymentstatusid = 1;

            $batch = get_batch();
            $nextpaymentid = get_paymentidcounter();
            $paymentid = "RMD-" . $batch . "-" . str_pad($nextpaymentid, 6, '0', STR_PAD_LEFT);

            $sql = "INSERT INTO payments(paymentid, userid, paymentamount, paymentdate, paymenttypeid, otherdetails, paymentcollectedby, paymentstatusid) 
                             VALUES(:paymentid,:userid,:paymentamount,:paymentdate,:paymenttypeid,:otherdetails,:paymentcollectedby,:paymentstatusid)";
            $query = $dbh->prepare($sql);
            $query->bindParam(':paymentid', $paymentid, PDO::PARAM_STR);
            $query->bindParam(':userid', $userid, PDO::PARAM_STR);
            $query->bindParam(':paymentamount', $paymentamount, PDO::PARAM_STR);
            $query->bindParam(':paymentdate', $paymentdate, PDO::PARAM_STR);
            $query->bindParam(':paymenttypeid', $paymenttypeid, PDO::PARAM_STR);
            $query->bindParam(':otherdetails', $otherdetails, PDO::PARAM_STR);
            $query->bindParam(':paymentcollectedby', $paymentcollectedby, PDO::PARAM_STR);
            $query->bindParam(':paymentstatusid', $paymentstatusid, PDO::PARAM_STR);
            $issuccess = $query->execute();

            if ($issuccess) {
                set_paymentidcounter();
            }
        }
    }
?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>

            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Bulk Upload Employees
                                <a class="btn btn-warning" style="float: right;" href="employees_bulk_upload_sample.xls" download>Download Sample</a>
                            </h2>
                            <div class="row">
                                <form enctype='multipart/form-data' method="post">
                                    <div class="col-md-8">
                                        <input type="file" id="employees_upload" name="employees_upload" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" id="employees_bulk_upload" name="employees_bulk_upload" class="btn btn-primary">Upload</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Bulk Upload Students
                                <a class="btn btn-warning" style="float: right;" href="student_bulk_upload_sample.xls" download>Download Sample</a>
                            </h2>
                            <div class="row">
                                <form enctype='multipart/form-data' method="post">
                                    <div class="col-md-8">
                                        <input type="file" id="students_upload" name="students_upload" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" id="student_bulk_upload" name="student_bulk_upload" class="btn btn-primary">Upload</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Bulk Upload Payments
                                <a class="btn btn-warning" style="float: right;" href="payments_bulk_upload_sample.xls" download>Download Sample</a>
                            </h2>
                            <div class="row">
                                <form enctype='multipart/form-data' method="post">
                                    <div class="col-md-8">
                                        <input type="file" id="payments_upload" name="payments_upload" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" id="payment_bulk_upload" name="payment_bulk_upload" class="btn btn-primary">Upload</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Loading Scripts -->
        <script>
            window.onload = function() {
                $('.DataTable').DataTable();
                $('.select2').select2();
            }
        </script>
    </body>

    </html>
<?php endif; ?>