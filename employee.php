<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

if ($_SESSION['userdetails']->userid == 1) :

    if (isset($_POST['createotherusers'])) {
        $name = $_POST['name'];
        $branchid = COUNT($_POST['branchid']) == 0 ? "" : implode(',', $_POST['branchid']);
        $roleid = $_POST['roleid'];
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        $designation = $_POST['designation'];
        $blood_group = $_POST['blood_group'];
        $employee_id = $_POST['employee_id'];
        $email = $_POST['email'];


        if ($branchid == "") {
            $sql = "INSERT INTO users(username, password, roleid, name, designation, blood_group, employeeid, email) VALUES(:username, :password, :roleid, :name, :designation, :bloodgroup, :employee_id, :email)";
        } else {
            $sql = "INSERT INTO users(username, password, roleid, name, branchid, designation, bloodgroup, employeeid, email) VALUES(:username, :password, :roleid, :name, :branchid, :designation, :blood_group, :employee_id, :email)";
        }

        $query = $dbh->prepare($sql);
        $query->bindParam(':username', $username, PDO::PARAM_STR);
        $query->bindParam(':password', $password, PDO::PARAM_STR);
        $query->bindParam(':roleid', $roleid, PDO::PARAM_STR);
        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->bindParam(':designation', $designation, PDO::PARAM_STR);
        $query->bindParam(':blood_group', $blood_group, PDO::PARAM_STR);
        $query->bindParam(':employee_id', $employee_id, PDO::PARAM_STR);
        $query->bindParam(':email', $email, PDO::PARAM_STR);

        if ($branchid == "") {
        } else {
            $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
        }
        $query->execute();

        //------------Mail Function start----------------//
        $to = $email;
        $subject = 'MAIDENDROPGROUP EMPLOYEE';
        $message = 'Dear Employee Your Account Has Been Created.';
        $headers = 'From: vamsi.prudhvi07@gmail.com' . "\r\n" .
            'Reply-To: vamsi.prudhvi07@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        //PHP Mail function return boolean value
        mail($to, $subject, $message, $headers);


        //------------Mail Function Ends----------------//


    }

?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>

            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Employee
                                <a class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#addotherusers">Add Employee</a>
                            </h2>
                            <form action="" method="POST" name="filter_form">
                                <div class="form-group row">
                                    <div class="col-xs-2">
                                        <label for="ex1">Branch</label>
                                        <select name="filter_branchname" class="form-control">
                                            <option value="">Select Branch</option>
                                            <?php
                                            foreach ($branchlookup as $branch) :
                                            ?>
                                                <option value="<?php echo $branch->branchid; ?>"><?php echo $branch->city . ", " . $branch->campus; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="ex1">Role</label>
                                        <select name="filter_role" class="form-control">
                                            <option value="">Select Role</option>
                                            <?php
                                            foreach ($roleslookup as $role) :
                                            ?>
                                                <option value="<?php echo $role->roleid; ?>"><?php echo $role->rolename; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <br>
                                        <button type="submit" name="filter" id="filter" class="btn btn-info">Filter</button>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tblotherusers" class="DataTable table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id's</th>
                                                <th>Employee Id</th>
                                                <th>Contact</th>
                                                <th>Name</th>
                                                <th>Branch Name</th>
                                                <th>Role Name</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                                <th hidden></th>
                                                <th hidden></th>
                                                <th hidden></th>
                                                <th hidden></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($_POST['filter'])) {
                                                $filter_branchname = $_POST['filter_branchname'];
                                                $filter_role = $_POST['filter_role'];


                                                if ($filter_branchname == "" and $filter_role == "") {

                                                    $sql = "SELECT users.mobile1,users.employeeid,users.username,users.password,users.roleid,users.branchid,users.userid,users.name, roleslookup.rolename from users JOIN roleslookup ON roleslookup.roleid = users.roleid WHERE users.roleid NOT IN (5)";
                                                } elseif ($filter_branchname != "") {
                                                    $sql = "SELECT users.mobile1,users.employeeid,users.username,users.password,users.roleid,users.branchid,users.userid,users.name, roleslookup.rolename from users JOIN roleslookup ON roleslookup.roleid = users.roleid WHERE users.roleid NOT IN (5) AND users.branchid LIKE '%{$filter_branchname}%'";
                                                } elseif ($filter_role != "") {
                                                    $sql = "SELECT users.mobile1,users.employeeid,users.username,users.password,users.roleid,users.branchid,users.userid,users.name, roleslookup.rolename from users JOIN roleslookup ON roleslookup.roleid = users.roleid WHERE users.roleid NOT IN (5) AND users.roleid=$filter_role";
                                                }
                                            } else {
                                                $sql = "SELECT users.mobile1,users.employeeid,users.username,users.password,users.roleid,users.branchid,users.userid,users.name, roleslookup.rolename from users JOIN roleslookup ON roleslookup.roleid = users.roleid WHERE users.roleid NOT IN (5)";
                                            }
                                            $query = $dbh->prepare($sql);
                                            $query->execute();
                                            $results = $query->fetchAll(PDO::FETCH_OBJ);

                                            foreach ($results as $result) :
                                                $branchids = explode(',', $result->branchid);
                                                $branches = "";
                                                if ($result->branchid != null) {
                                                    foreach ($branchids as $branchid) {

                                                        $sql = "SELECT branchid, city, campus from branchlookup where branchid = '{$branchid}'";
                                                        $query = $dbh->prepare($sql);
                                                        $query->execute();
                                                        $results1 = $query->fetchAll(PDO::FETCH_OBJ);

                                                        $branches .= $results1[0]->city . "-" . $results1[0]->campus . ', ';
                                                    }
                                                }

                                            ?>
                                                <tr>
                                                    <td><?php echo $result->userid ?></td>
                                                    <td><?php echo $result->employeeid ?></td>
                                                    <td><?php echo $result->mobile1 ?></td>
                                                    <td><?php echo $result->name ?></td>
                                                    <td><?php echo rtrim($branches, ', '); ?></td>
                                                    <td><?php echo $result->rolename ?></td>
                                                    <td><i data-toggle="modal" data-target="#editotherusers" class="fa fa-pencil-square-o editbtn"></i></td>
                                                    <td><?php
                                                        $html = '<a href="' . delete_otherusers() . '?admin_id=' . $result->userid . '" class="btn-del"><i class="fa fa-trash"></i></a>';
                                                        echo $html; ?></i></td>
                                                    <td hidden><?php echo $result->username ?></td>
                                                    <td hidden><?php echo $result->password ?></td>
                                                    <td hidden><?php echo $result->branchid ?></td>
                                                    <td hidden><?php echo $result->roleid ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="addotherusers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Add Employee</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="" class="text-uppercase text-sm">Name</label>
                                                    <input type="text" placeholder="Name" name="name" class="form-control mb" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Branch</label>
                                                    <select name="branchid[]" style="width: 100%;" class="select2 form-control mb" multiple>
                                                        <?php
                                                        foreach ($branchlookup as $branch) :
                                                        ?>
                                                            <option value="<?php echo $branch->branchid; ?>"><?php echo $branch->city . ", " . $branch->campus; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>

                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Role</label>
                                                    <select name="roleid" style="width: 100%;" class="select2 form-control mb" required>
                                                        <option value="">Select Role</option>
                                                        <?php
                                                        foreach ($roleslookup as $role) :
                                                        ?>
                                                            <option value="<?php echo $role->roleid; ?>"><?php echo $role->rolename; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>

                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Username</label>
                                                    <input type="text" placeholder="Username" name="username" class="form-control mb" required>

                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Password</label>
                                                    <input type="text" placeholder="Password" name="password" class="form-control mb" required>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Designation</label>
                                                    <input type="text" placeholder="Designation" name="designation" class="form-control mb" required>

                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Blood Group</label>
                                                    <input type="text" placeholder="Blood Group" name="blood_group" class="form-control mb" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Employee Id</label>
                                                    <input type="text" placeholder="Employee Id" name="employee_id" class="form-control mb" required>

                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">E-Mail</label>
                                                    <input type="email" placeholder="Email ID" name="email" class="form-control mb" required>

                                                </div>
                                            </div>

                                            <button type="submit" name="createotherusers" class="btn btn-primary">Create Employee</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="editotherusers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Edit Employee</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="" class="text-uppercase text-sm">Name</label>
                                                    <input id="name" type="text" placeholder="Name" name="name" class="form-control mb" required>
                                                    <input type="text" name="id" id="id" hidden>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Branch</label>
                                                    <select id="branchid" name="branchid[]" style="width: 100%;" class="select2 form-control mb" multiple>
                                                        <option value=""></option>
                                                        <?php
                                                        foreach ($branchlookup as $branch) :
                                                        ?>
                                                            <option value="<?php echo $branch->branchid; ?>"><?php echo $branch->city . ", " . $branch->campus; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Role</label>
                                                    <select id="roleid" name="roleid" style="width: 100%;" class="select2 form-control mb">
                                                        <option value=""></option>

                                                        <?php
                                                        foreach ($roleslookup as $role) :
                                                        ?>
                                                            <option value="<?php echo $role->roleid; ?>"><?php echo $role->rolename; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Username</label>
                                                    <input id="username" type="text" placeholder="Username" name="username" class="form-control mb" required>

                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Password</label>
                                                    <input type="text" placeholder="Password" name="password" class="form-control mb">
                                                    <input id="password" type="text" placeholder="Password" name="password_hidden" hidden>
                                                </div>
                                            </div>
                                            <button type="submit" name="update_otherusers" class="btn btn-primary">Update Employee</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Loading Scripts -->
        <script>
            window.onload = function() {
                $('.DataTable').DataTable();
                $('.select2').select2();
            }
        </script>
    </body>

    </html>
<?php endif; ?>
<script>
    window.onload = function() {
        $('.DataTable').DataTable();
        $('.select2').select2();
    }
</script>
<script>
    $('.btn-del').on('click', function(e) {
        e.preventDefault();
        const href = $(this).attr('href')

        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.location.href = href;
                }
            })

    })
</script>

<script>
    $(document).ready(function() {
        $('.editbtn').on('click', function() {
            $('#myModal').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function() {
                return $(this).text();
            }).get();

            $('#id').val(data[0]);
            $('#name').val(data[3]);
            $('#username').val(data[8]);
            $('#password').val(data[9]);
            $('#branchid').val(data[10].split(',')).change();
            $('#branchid').select2();
            $('#roleid').select2("val", data[11]);
        });
    });
</script>


<?php
function delete_otherusers()
{
    $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

    if (isset($_GET['admin_id'])) {
        $id = $_GET['admin_id'];
        echo $id;

        $query = $dbh->prepare("DELETE FROM users WHERE userid=:id");
        $query->bindParam(":id", $id, PDO::PARAM_STR);
        $query->execute();

        echo "<script>window.location.href = \"employee.php\";</script>";
    }
}

?>
<?php
$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
if (isset($_POST['update_otherusers'])) {
    $name = $_POST['name'];
    $userid = $_POST['id'];
    $username = $_POST['username'];
    $branch = COUNT($_POST['branchid']) == 0 ? "" : implode(',', $_POST['branchid']);
    $role = $_POST['roleid'];

    if ($_POST['password'] == "") {
        $password = $_POST['password_hidden'];
    } else {
        $password = md5($_POST['password']);
    }

    $sql = "UPDATE users SET username='$username',password='$password',roleid='$role',name='$name',branchid='$branch' WHERE userid='$userid'";

    $query = $dbh->prepare($sql);

    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->bindParam(':username', $username, PDO::PARAM_STR);
    $query->bindParam(':password', $password, PDO::PARAM_STR);
    $query->bindParam(':roleid', $roleid, PDO::PARAM_STR);
    $query->bindParam(':name', $name, PDO::PARAM_STR);
    $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);

    $query->execute();
    echo "<script>window.location.href = \"employee.php\";</script>";
} ?>