<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

if ($_SESSION['userdetails'] == NULL) {
	header('location:index.php');
} else {
	if (isset($_POST['changepassword'])) {
		$oldpassword = md5($_POST['oldpassword']);
		$newpassword = md5($_POST['newpassword']);

		$sql = "SELECT * from users where password = '{$oldpassword}' and userid = {$_SESSION['userdetails']->userid}";
		$query = $dbh->prepare($sql);
		$query->execute();
		$results = $query->fetchAll(PDO::FETCH_OBJ);

		if (count($results) > 0) {
			$sql = "UPDATE users SET password = '{$newpassword}' WHERE userid = {$_SESSION['userdetails']->userid}";

			$query = $dbh->prepare($sql);
			$query->bindParam(':userid', $userid, PDO::PARAM_STR);
			$query->bindParam(':name', $name, PDO::PARAM_STR);
			$query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
			$query->execute();

			echo "<script type='text/javascript'>alert('Password Changed Successfully')</script>";
		} else {
			echo "<script type='text/javascript'>alert('Invalid Old Password')</script>";
		}
	}
?>
	<!doctype html>
	<html lang="en" class="no-js">

	<head>
		<?php include('includes/header.php'); ?>
	</head>

	<body>
		<div class="ts-main-content">
			<?php include('includes/leftbar.php'); ?>
			<div class="content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<h2 class="page-title">Reports</h2>
							<form method="post">
								<button type="submit" name="download employees" class="btn btn-primary">Download Employees</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	</html>
<?php } ?>