<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

$pageaccess = $_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))];
if ($pageaccess->_view != 1) {
    header('location:index.php');
} else {

    if (isset($_POST['createstudent'])) {
        $name = $_POST['name'];
        $fathername = $_POST['fathername'];
        $mothername = $_POST['mothername'];
        $parentoccupation = $_POST['parentoccupation'];
        $dateofbirth = $_POST['dateofbirth'];
        $genderid = $_POST['genderid'];
        $studentaadhaar = $_POST['studentaadhaar'];
        $nationalityid = $_POST['nationalityid'];
        $religionid = $_POST['religionid'];
        $categoryid = $_POST['categoryid'];
        $boardid = $_POST['boardid'];
        $hallticket = $_POST['hallticket'];
        $schoolname = $_POST['schoolname'];
        $grade = $_POST['grade'];
        $placeofeducation = $_POST['placeofeducation'];
        $door_street = $_POST['door_street'];
        $village_mandal = $_POST['village_mandal'];
        $landmark = $_POST['landmark'];
        $city_town = $_POST['city_town'];
        $district = $_POST['district'];
        $pin = $_POST['pin'];
        $mobile1 = $_POST['mobile1'];
        $mobile2 = $_POST['mobile2'];
        $email = $_POST['email'];
        $admissiontypeid = $_POST['admissiontypeid'];
        $applicationnumber = $_POST['applicationnumber'];
        $courseid = $_POST['courseid'];
        $termid = $_POST['termid'];
        $secondlanguageid = $_POST['secondlanguageid'];
        $branchid = $_POST['branchid'];
        $coachingfee = $_POST['coachingfee'];
        $comments = $_POST['comments'];
        $referredby = $_POST['referredby'] == "" ? 0 : $_POST['referredby'];

        $now = new DateTime();
        $username = "USR-" . $now->format('dmYHisu');
        $password = md5($username);
        $roleid = 5;

        if ((float) $coachingfee <= (float) $approvalvaluelookup[0]->superadminapproval) {
            $sendapprovalto = 'SA';
            $applicationstatusid = 1;
        } else {
            $sendapprovalto = 'CH';
            $applicationstatusid = 1;
        }

        $sql = "INSERT INTO users (username, password, roleid, name, fathername, mothername, parentoccupation, studentaadhaar, boardid, hallticket, dateofbirth, nationalityid, religionid, categoryid, schoolname, grade, placeofeducation, genderid, admissiontypeid, branchid, courseid, termid, secondlanguageid, door_street, village_mandal, landmark, city_town, district, pin, mobile1, mobile2, email, applicationstatusid, applicationnumber, sendapprovalto, coachingfee, comments, referredby) 
                          VALUES (:username,:password,:roleid,:name,:fathername,:mothername,:parentoccupation,:studentaadhaar,:boardid,:hallticket,:dateofbirth,:nationalityid,:religionid,:categoryid,:schoolname,:grade,:placeofeducation,:genderid,:admissiontypeid,:branchid,:courseid,:termid,:secondlanguageid,:door_street,:village_mandal,:landmark,:city_town,:district,:pin,:mobile1,:mobile2,:email,:applicationstatusid,:applicationnumber,:sendapprovalto,:coachingfee,:comments,:referredby)";

        $query = $dbh->prepare($sql);
        $query->bindParam(':username', $username, PDO::PARAM_STR);
        $query->bindParam(':password', $password, PDO::PARAM_STR);
        $query->bindParam(':roleid', $roleid, PDO::PARAM_STR);
        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->bindParam(':fathername', $fathername, PDO::PARAM_STR);
        $query->bindParam(':mothername', $mothername, PDO::PARAM_STR);
        $query->bindParam(':parentoccupation', $parentoccupation, PDO::PARAM_STR);
        $query->bindParam(':studentaadhaar', $studentaadhaar, PDO::PARAM_STR);
        $query->bindParam(':boardid', $boardid, PDO::PARAM_STR);
        $query->bindParam(':hallticket', $hallticket, PDO::PARAM_STR);
        $query->bindParam(':dateofbirth', $dateofbirth, PDO::PARAM_STR);
        $query->bindParam(':nationalityid', $nationalityid, PDO::PARAM_STR);
        $query->bindParam(':religionid', $religionid, PDO::PARAM_STR);
        $query->bindParam(':categoryid', $categoryid, PDO::PARAM_STR);
        $query->bindParam(':schoolname', $schoolname, PDO::PARAM_STR);
        $query->bindParam(':grade', $grade, PDO::PARAM_STR);
        $query->bindParam(':placeofeducation', $placeofeducation, PDO::PARAM_STR);
        $query->bindParam(':genderid', $genderid, PDO::PARAM_STR);
        $query->bindParam(':admissiontypeid', $admissiontypeid, PDO::PARAM_STR);
        $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
        $query->bindParam(':courseid', $courseid, PDO::PARAM_STR);
        $query->bindParam(':termid', $termid, PDO::PARAM_STR);
        $query->bindParam(':secondlanguageid', $secondlanguageid, PDO::PARAM_STR);
        $query->bindParam(':door_street', $door_street, PDO::PARAM_STR);
        $query->bindParam(':village_mandal', $village_mandal, PDO::PARAM_STR);
        $query->bindParam(':landmark', $landmark, PDO::PARAM_STR);
        $query->bindParam(':city_town', $city_town, PDO::PARAM_STR);
        $query->bindParam(':district', $district, PDO::PARAM_STR);
        $query->bindParam(':pin', $pin, PDO::PARAM_STR);
        $query->bindParam(':mobile1', $mobile1, PDO::PARAM_STR);
        $query->bindParam(':mobile2', $mobile2, PDO::PARAM_STR);
        $query->bindParam(':email', $email, PDO::PARAM_STR);
        $query->bindParam(':applicationstatusid', $applicationstatusid, PDO::PARAM_STR);
        $query->bindParam(':applicationnumber', $applicationnumber, PDO::PARAM_STR);
        $query->bindParam(':sendapprovalto', $sendapprovalto, PDO::PARAM_STR);
        $query->bindParam(':coachingfee', $coachingfee, PDO::PARAM_STR);
        $query->bindParam(':comments', $comments, PDO::PARAM_STR);
        $query->bindParam(':referredby', $referredby, PDO::PARAM_STR);

        $query->execute();
        
        
//------------Mail Function start----------------//
$to = $email;
$subject = 'MAIDENDROPGROUP STUDENT';
$message = 'Dear Student Your Account Has Been Created.';
$headers = 'From: vamsi.prudhvi07@gmail.com' . "\r\n" .
        'Reply-To: vamsi.prudhvi07@gmail.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

//PHP Mail function return boolean value
mail($to, $subject, $message, $headers);


//------------Mail Function Ends----------------//
        
        header('location:student.php');
    }

    if (isset($_POST['updatestudent'])) {
        $userid = $_POST['Edituserid'];

        $name = $_POST['Editname'];
        $fathername = $_POST['Editfathername'];
        $mothername = $_POST['Editmothername'];
        $parentoccupation = $_POST['Editparentoccupation'];
        $dateofbirth = $_POST['Editdateofbirth'];
        $genderid = $_POST['Editgenderid'];
        $studentaadhaar = $_POST['Editstudentaadhaar'];
        $nationalityid = $_POST['Editnationalityid'];
        $religionid = $_POST['Editreligionid'];
        $categoryid = $_POST['Editcategoryid'];
        $boardid = $_POST['Editboardid'];
        $hallticket = $_POST['Edithallticket'];
        $schoolname = $_POST['Editschoolname'];
        $grade = $_POST['Editgrade'];
        $placeofeducation = $_POST['Editplaceofeducation'];
        $door_street = $_POST['Editdoor_street'];
        $village_mandal = $_POST['Editvillage_mandal'];
        $landmark = $_POST['Editlandmark'];
        $city_town = $_POST['Editcity_town'];
        $district = $_POST['Editdistrict'];
        $pin = $_POST['Editpin'];
        $mobile1 = $_POST['Editmobile1'];
        $mobile2 = $_POST['Editmobile2'];
        $email = $_POST['Editemail'];
        $admissiontypeid = $_POST['Editadmissiontypeid'];
        $courseid = $_POST['Editcourseid'];
        $termid = $_POST['Edittermid'];
        $secondlanguageid = $_POST['Editsecondlanguageid'];
        $branchid = $_POST['Editbranchid'];

        $sql = "UPDATE users SET name=:name, fathername=:fathername, mothername=:mothername, parentoccupation=:parentoccupation, studentaadhaar=:studentaadhaar, 
                                 boardid=:boardid, hallticket=:hallticket, dateofbirth=:dateofbirth, nationalityid=:nationalityid, religionid=:religionid, 
                                 categoryid=:categoryid, schoolname=:schoolname, grade=:grade, placeofeducation=:placeofeducation, genderid=:genderid, 
                                 admissiontypeid=:admissiontypeid, branchid=:branchid, courseid=:courseid, termid=:termid, secondlanguageid=:secondlanguageid, 
                                 door_street=:door_street, village_mandal=:village_mandal, landmark=:landmark, city_town=:city_town, district=:district, 
                                 pin=:pin, mobile1=:mobile1, mobile2=:mobile2, email=:email WHERE userid=:userid";

        $query = $dbh->prepare($sql);
        $query->bindParam(':userid', $userid, PDO::PARAM_STR);

        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->bindParam(':fathername', $fathername, PDO::PARAM_STR);
        $query->bindParam(':mothername', $mothername, PDO::PARAM_STR);
        $query->bindParam(':parentoccupation', $parentoccupation, PDO::PARAM_STR);
        $query->bindParam(':studentaadhaar', $studentaadhaar, PDO::PARAM_STR);
        $query->bindParam(':boardid', $boardid, PDO::PARAM_STR);
        $query->bindParam(':hallticket', $hallticket, PDO::PARAM_STR);
        $query->bindParam(':dateofbirth', $dateofbirth, PDO::PARAM_STR);
        $query->bindParam(':nationalityid', $nationalityid, PDO::PARAM_STR);
        $query->bindParam(':religionid', $religionid, PDO::PARAM_STR);
        $query->bindParam(':categoryid', $categoryid, PDO::PARAM_STR);
        $query->bindParam(':schoolname', $schoolname, PDO::PARAM_STR);
        $query->bindParam(':grade', $grade, PDO::PARAM_STR);
        $query->bindParam(':placeofeducation', $placeofeducation, PDO::PARAM_STR);
        $query->bindParam(':genderid', $genderid, PDO::PARAM_STR);
        $query->bindParam(':admissiontypeid', $admissiontypeid, PDO::PARAM_STR);
        $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
        $query->bindParam(':courseid', $courseid, PDO::PARAM_STR);
        $query->bindParam(':termid', $termid, PDO::PARAM_STR);
        $query->bindParam(':secondlanguageid', $secondlanguageid, PDO::PARAM_STR);
        $query->bindParam(':door_street', $door_street, PDO::PARAM_STR);
        $query->bindParam(':village_mandal', $village_mandal, PDO::PARAM_STR);
        $query->bindParam(':landmark', $landmark, PDO::PARAM_STR);
        $query->bindParam(':city_town', $city_town, PDO::PARAM_STR);
        $query->bindParam(':district', $district, PDO::PARAM_STR);
        $query->bindParam(':pin', $pin, PDO::PARAM_STR);
        $query->bindParam(':mobile1', $mobile1, PDO::PARAM_STR);
        $query->bindParam(':mobile2', $mobile2, PDO::PARAM_STR);
        $query->bindParam(':email', $email, PDO::PARAM_STR);

        $query->execute();
        header('location:student.php');
    }
?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js'></script>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Students
                                <?php if ($_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))]->_add == 1) : ?>
                                    <a class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#addstudent">Add Student</a>
                                <?php endif; ?>
                            </h2>

                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tblStudents" class="DataTable table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Application Number</th>
                                                <th>Student Name</th>
                                                <th>Application Status</th>
                                                <th>Gender</th>
                                                <th>Branch</th>
                                                <!--<th>Cource - Term - Admission Type</th>-->
                                                <th>Mobile</th>
                                                <th>Comments</th>
                                                <?php if ($_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))]->_edit == 1) : ?>
                                                    <th>Edit</th>
                                                <?php endif; ?>
                                                <?php if ($_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))]->_delete == 1) : ?>
                                                    <th>Delete</th>
                                                <?php endif; ?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($_SESSION['userdetails']->branchid == NULL) {
                                                $sql = "SELECT * from users 
                                                JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                JOIN courselookup ON users.courseid = courselookup.courseid
                                                JOIN applicationstatuslookup ON users.applicationstatusid = applicationstatuslookup.applicationstatusid
                                                JOIN genderlookup ON users.genderid = genderlookup.genderid
                                                JOIN termlookup ON users.termid = termlookup.termid
                                                WHERE roleid = 5";
                                                //  and users.applicationstatusid <> 2
                                            } else {
                                                $sql = "SELECT * from users 
                                                JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                JOIN courselookup ON users.courseid = courselookup.courseid
                                                JOIN applicationstatuslookup ON users.applicationstatusid = applicationstatuslookup.applicationstatusid
                                                JOIN genderlookup ON users.genderid = genderlookup.genderid
                                                JOIN termlookup ON users.termid = termlookup.termid
                                                WHERE roleid = 5 and branchlookup.branchid IN ({$_SESSION['userdetails']->branchid})";
                                                //  and users.applicationstatusid <> 2
                                            }

                                            $query = $dbh->prepare($sql);
                                            $query->execute();
                                            $results = $query->fetchAll(PDO::FETCH_OBJ);

                                            foreach ($results as $result) :
                                            ?>
                                                <tr>
                                                    <td><?php echo $result->applicationnumber ?></td>
                                                    <td><a href="student_details.php?student_id=<?php echo $result->userid; ?>"><?php echo $result->name ?></a></td>
                                                    <td><?php echo $result->applicationstatusname . "({$result->sendapprovalto})" ?></td>
                                                    <td><?php echo $result->gendername ?></td>
                                                    <td><?php echo $result->city . ", " . $result->campus ?></td>
                                                    <!--<td><?php echo $result->coursename . ", " . $result->termname . ', ' . $result->admissiontypename ?></td>-->
                                                    <td><?php echo $result->mobile1 . "<br />" . $result->mobile2 ?></td>
                                                    <td><?php echo $result->comments; ?></td>

                                                    <?php if ($_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))]->_edit == 1) : ?>
                                                        <td>
                                                            <a data-toggle="modal" onclick="loadEditControls(<?php echo $result->userid; ?>)" data-target="#editstudent"><i class="fa fa-pencil-square-o"></i></a>
                                                        </td>
                                                    <?php endif; ?>

                                                    <?php if ($_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))]->_delete == 1) : ?>
                                                        <td><i class="fa fa-trash"></i></td>
                                                    <?php endif; ?>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="addstudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document" style="width: 80vw">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Add Student</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" id="studentform">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a id="BasicTab" data-toggle="tab" href="#basic">Basic Details</a></li>
                                                <li><a id="FamilyTab" data-toggle="tab" href="#family">Family Details</a></li>
                                                <li><a id="EducationTab" data-toggle="tab" href="#education">Education Details</a></li>
                                                <li><a id="ContactTab" data-toggle="tab" href="#contact">Contact Details</a></li>
                                                <li><a id="ApplicationTab" data-toggle="tab" href="#application">Application Details</a></li>
                                                <li><a id="FeesTab" data-toggle="tab" href="#fees">Fee Details</a></li>
                                                <li><a data-toggle="tab" href="#preview" onclick="generatePreview()">Preview and Create</a></li>
                                            </ul>

                                            <div class="tab-content tab-validate">
                                                <div id="basic" class="tab-pane fade in active">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Student Name</label>
                                                            <input type="text" placeholder="Name" id="name" name="name" class="form-control mb" required>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Date Of Birth</label>
                                                            <input type="text" placeholder="yyyy-mm-dd" id="dateofbirth" name="dateofbirth" class="form-control mb datepicker" required>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="genderid" name="genderid" style="width: 100%;" class="select2 form-control mb select" required>
                                                                <option value="">Select Gender</option>
                                                                <?php
                                                                foreach ($genderlookup as $gender) :
                                                                ?>
                                                                    <option value="<?php echo $gender->genderid; ?>"><?php echo $gender->gendername; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="nationalityid" name="nationalityid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Nationality</option>
                                                                <?php
                                                                foreach ($nationalitylookup as $nationality) :
                                                                ?>
                                                                    <option value="<?php echo $nationality->nationalityid; ?>"><?php echo $nationality->nationalityname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="religionid" name="religionid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Religion</option>
                                                                <?php
                                                                foreach ($religionlookup as $religion) :
                                                                ?>
                                                                    <option value="<?php echo $religion->religionid; ?>"><?php echo $religion->religionname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="categoryid" name="categoryid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Category</option>
                                                                <?php
                                                                foreach ($categorylookup as $category) :
                                                                ?>
                                                                    <option value="<?php echo $category->categoryid; ?>"><?php echo $category->categoryname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Aadhaar Number</label>
                                                            <input type="text" placeholder="Aadhaar Number" id="studentaadhaar" name="studentaadhaar" class="form-control mb" required>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="family" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Father Name/ Guardian Name</label>
                                                            <input type="text" placeholder="Father Name" id="fathername" name="fathername" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Mother Name</label>
                                                            <input type="text" placeholder="Mother Name" id="mothername" name="mothername" class="form-control mb" required>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Parent Occupation</label>
                                                            <input type="text" placeholder="Parent Occupation" id="parentoccupation" name="parentoccupation" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="education" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label for="" class="text-uppercase text-sm">Which board did the student study in?</label>
                                                            <select onchange="this.classList.remove('error')" id="boardid" name="boardid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Board</option>
                                                                <?php
                                                                foreach ($boardlookup as $board) :
                                                                ?>
                                                                    <option value="<?php echo $board->boardid; ?>"><?php echo $board->boardname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">School Name</label>
                                                            <input type="text" placeholder="School Name" id="schoolname" name="schoolname" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Grade/ Marks</label>
                                                            <input type="text" placeholder="Grade/Marks" id="grade" name="grade" class="form-control mb" required>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Hallticket Number</label>
                                                            <input type="text" placeholder="Hallticket Number" id="hallticket" name="hallticket" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Area/ Place</label>
                                                            <input type="text" placeholder="Place of education" id="placeofeducation" name="placeofeducation" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="contact" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Door No./ Street Name</label>
                                                            <input type="text" placeholder="Door No./ Street Name" id="door_street" name="door_street" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Village/ Mandal</label>
                                                            <input type="text" placeholder="Village/ Mandal" id="village_mandal" name="village_mandal" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Landmark</label>
                                                            <input type="text" placeholder="Landmark" id="landmark" name="landmark" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">City/ Town</label>
                                                            <input type="text" placeholder="City/ Town" id="city_town" name="city_town" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">District</label>
                                                            <input type="text" placeholder="District" id="district" name="district" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Pin</label>
                                                            <input type="text" placeholder="Pin" id="pin" name="pin" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Mobile 1</label>
                                                            <input type="number" placeholder="Mobile 1" id="mobile1" name="mobile1" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Mobile 2</label>
                                                            <input type="number" placeholder="Mobile 2" id="mobile2" name="mobile2" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label for="" class="text-uppercase text-sm">Email Address</label>
                                                            <input type="text" placeholder="Email Address" id="email" name="email" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="application" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label for="" class="text-uppercase text-sm">Application Number</label>
                                                            <input type="text" placeholder="Application Number" id="applicationnumber" name="applicationnumber" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error');changefees(this.value)" id="admissiontypeid" name="admissiontypeid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Admission Type</option>
                                                                <?php
                                                                foreach ($admissiontypelookup as $admissiontype) :
                                                                ?>
                                                                    <option value="<?php echo $admissiontype->admissiontypeid; ?>"><?php echo $admissiontype->admissiontypename; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="branchid" name="branchid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Branch</option>
                                                                <?php
                                                                foreach ($branchlookup as $branch) :
                                                                ?>
                                                                    <option value="<?php echo $branch->branchid; ?>"><?php echo $branch->city . ", " . $branch->campus; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="courseid" name="courseid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Course</option>
                                                                <?php
                                                                foreach ($courselookup as $course) :
                                                                ?>
                                                                    <option value="<?php echo $course->courseid; ?>"><?php echo $course->coursename; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="termid" name="termid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Term</option>
                                                                <?php
                                                                foreach ($termlookup as $term) :
                                                                ?>
                                                                    <option value="<?php echo $term->termid; ?>"><?php echo $term->termname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="secondlanguageid" name="secondlanguageid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Second Language</option>
                                                                <?php
                                                                foreach ($secondlanguagelookup as $secondlanguage) :
                                                                ?>
                                                                    <option value="<?php echo $secondlanguage->secondlanguageid; ?>"><?php echo $secondlanguage->secondlanguagename; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" name="referredby" style="width: 100%;" class="select2 form-control mb">
                                                                <option value="">Select Reference</option>
                                                                <?php
                                                                $sql = "SELECT * from users WHERE roleid <> 5";
                                                                $query = $dbh->prepare($sql);
                                                                $query->execute();
                                                                $references = $query->fetchAll(PDO::FETCH_OBJ);

                                                                foreach ($references as $reference) :
                                                                ?>
                                                                    <option value="<?php echo $reference->userid; ?>"><?php echo $reference->name; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="fees" class="tab-pane fade">
                                                    <br>
                                                    <div id="selectadmissiontype">
                                                        <h4>Select Admission Type to View Fees</h4>
                                                    </div>
                                                    <div id="dayscholarfees" style="display: none;">
                                                        <?php
                                                        $coachFees = 0;
                                                        $otherfees = 0;
                                                        foreach ($feestructurelookup as $feestructure) {
                                                            if ($feestructure->admissiontypename == "Day Scholar") {

                                                                if ($feestructure->canchange == 1) : ?>
                                                                    <?php $coachFees = $feestructure->feesvalue ?>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <?php echo $feestructure->feetype ?>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <input type='number' onchange="dschange();" onkeyup="dschange();" id="dscoaching" class='form-control mb' value='<?php echo $feestructure->feesvalue ?>'>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <input type='text' onkeyup="$('#comments').val(this.value)" placeholder="comments" class='form-control mb'>
                                                                        </div>
                                                                    </div>
                                                                <?php else : ?>
                                                                    <div class="row">
                                                                        <?php $otherfees = $otherfees + $feestructure->feesvalue ?>
                                                                        <div class="col-md-4">
                                                                            <?php echo $feestructure->feetype ?>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <b><?php echo $feestructure->feesvalue ?></b>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            &nbsp;
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>

                                                        <?php                }
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                Total Fees
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="hidden" id="dsother" value="<?php echo $otherfees; ?>" />
                                                                <b><span id="dstotal" style="color: green;"><?php echo $coachFees + $otherfees ?></span></b>
                                                            </div>
                                                            <div class="col-md-4">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="semiresidentialfees" style="display: none;">
                                                        <?php
                                                        $coachFees = 0;
                                                        $otherfees = 0;
                                                        foreach ($feestructurelookup as $feestructure) {
                                                            if ($feestructure->admissiontypename == "Semi Residential") {

                                                                if ($feestructure->canchange == 1) : ?>
                                                                    <?php $coachFees = $feestructure->feesvalue ?>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <?php echo $feestructure->feetype ?>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <input type='number' onchange="srchange();" onkeyup="srchange();" id="srcoaching" class='form-control mb' value='<?php echo $feestructure->feesvalue ?>'>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <input type='text' onkeyup="$('#comments').val(this.value)" placeholder="comments" class='form-control mb'>
                                                                        </div>
                                                                    </div>
                                                                <?php else : ?>
                                                                    <div class="row">
                                                                        <?php $otherfees = $otherfees + $feestructure->feesvalue ?>
                                                                        <div class="col-md-4">
                                                                            <?php echo $feestructure->feetype ?>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <b><?php echo $feestructure->feesvalue ?></b>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            &nbsp;
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>

                                                        <?php                }
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                Total Fees
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="hidden" id="srother" value="<?php echo $otherfees; ?>" />
                                                                <b><span id="srtotal"><?php echo $coachFees + $otherfees ?></span></b>
                                                            </div>
                                                            <div class="col-md-4">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="residentialfees" style="display: none;">
                                                        <?php
                                                        $coachFees = 0;
                                                        $otherfees = 0;
                                                        foreach ($feestructurelookup as $feestructure) {
                                                            if ($feestructure->admissiontypename == "Residential") {

                                                                if ($feestructure->canchange == 1) : ?>
                                                                    <?php $coachFees = $feestructure->feesvalue ?>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <?php echo $feestructure->feetype ?>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <input type='number' onchange="rdchange();" onkeyup="rdchange();" id="rdcoaching" class='form-control mb' value='<?php echo $feestructure->feesvalue ?>'>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <input type='text' onkeyup="$('#comments').val(this.value)" placeholder="comments" class='form-control mb'>
                                                                        </div>
                                                                    </div>
                                                                <?php else : ?>
                                                                    <div class="row">
                                                                        <?php $otherfees = $otherfees + $feestructure->feesvalue ?>
                                                                        <div class="col-md-4">
                                                                            <?php echo $feestructure->feetype ?>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <b><?php echo $feestructure->feesvalue ?></b>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            &nbsp;
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>

                                                        <?php                }
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                Total Fees
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="hidden" id="rdother" value="<?php echo $otherfees; ?>" />
                                                                <b><span id="rdtotal"><?php echo $coachFees + $otherfees ?></span></b>
                                                            </div>
                                                            <div class="col-md-4">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                        <br />
                                                    </div>

                                                </div>
                                                <div id="preview" class="tab-pane fade">
                                                    <div id="print_preview">
                                                        <table style="width: 100%;margin: 0;">
                                                            <tr>
                                                                <th style="float: left !important;"><br /><i>Basic Details</i></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Student Name: <b><span id="preview_studentname"></span></b></td>
                                                                <td>Date Of Birth: <b><span id="preview_dateofbirth"></span></b></td>
                                                                <td>Gender: <b><span id="preview_gender"></span></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nationality: <b><span id="preview_nationality"></span></b></td>
                                                                <td>Religion: <b><span id="preview_religion"></span></b></td>
                                                                <td>Category: <b><span id="preview_category"></span></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Aadhaar Number: <b><span id="preview_aadhaar"></span></b></td>
                                                            </tr>

                                                            <tr>
                                                                <th style="float: left !important;"><br /><i>Family Details</i></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Father Name/ Guardian Name: <b><span id="preview_fathername"></span></b></td>
                                                                <td>Mother Name: <b><span id="preview_mothername"></span></b></td>
                                                                <td>Parent Occupation: <b><span id="preview_parentoccupation"></span></b></td>
                                                            </tr>

                                                            <tr>
                                                                <th style="float: left !important;"><br /><i>Education Details</i></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Board Name: <b><span id="preview_board"></span></b></td>
                                                                <td>School Name: <b><span id="preview_schoolname"></span></b></td>
                                                                <td>Grade/ Marks: <b><span id="preview_grade"></span></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Hallticket Number: <b><span id="preview_hallticketnumber"></span></b></td>
                                                                <td>Area/ Place: <b><span id="preview_area"></span></b></td>
                                                            </tr>

                                                            <tr>
                                                                <th style="float: left !important;"><br /><i>Contact Details</i></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Door No./ Street Name: <b><span id="preview_doorno"></span></b></td>
                                                                <td>Village/ Mandal: <b><span id="preview_village"></span></b></td>
                                                                <td>Landmark: <b><span id="preview_landmark"></span></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>City/ Town: <b><span id="preview_city"></span></b></td>
                                                                <td>District: <b><span id="preview_district"></span></b></td>
                                                                <td>Pin: <b><span id="preview_pin"></span></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mobile1: <b><span id="preview_mobile1"></span></b></td>
                                                                <td>Mobile2: <b><span id="preview_mobile2"></span></b></td>
                                                                <td>Email Address: <b><span id="preview_email"></span></b></td>
                                                            </tr>

                                                            <tr>
                                                                <th style="float: left !important;"><br /><i>Application Details</i></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Application Number: <b><span id="preview_applicationnumber"></span></b></td>
                                                                <td>Admission Type: <b><span id="preview_admissiontype"></span></b></td>
                                                                <td>Branch: <b><span id="preview_branch"></span></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Course: <b><span id="preview_course"></span></b></td>
                                                                <td>Term: <b><span id="preview_term"></span></b></td>
                                                                <td>Second Language: <b><span id="preview_secondlanguage"></span></b></td>
                                                            </tr>

                                                            <tr>
                                                                <th style="float: left !important;"><br /><i>Fee Details</i></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Coaching Fee: <b><span id="preview_coachingfee"></span></b></td>
                                                                <td>Comments: <b><span id="preview_comments"></span></b></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <br />
                                                    <button type="submit" name="createstudent" class="btn btn-primary">Create Student</button>
                                                    <a class="btn btn-primary" onclick="print_preview()">Print Application</a>
                                                </div>
                                            </div>
                                            <!-- <button type="submit" id="createstudent" name="createstudent" class="btn btn-primary">Create Student</button> -->
                                            <input type="hidden" id="coachingfee" name='coachingfee' required />
                                            <input type="hidden" id="comments" name='comments' />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="editstudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document" style="width: 80vw">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Edit Student</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" id="editstudentform">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a id="EditBasicTab" data-toggle="tab" href="#Editbasic">Basic Details</a></li>
                                                <li><a id="EditFamilyTab" data-toggle="tab" href="#Editfamily">Family Details</a></li>
                                                <li><a id="EditEducationTab" data-toggle="tab" href="#Editeducation">Education Details</a></li>
                                                <li><a id="EditContactTab" data-toggle="tab" href="#Editcontact">Contact Details</a></li>
                                                <li><a id="EditApplicationTab" data-toggle="tab" href="#Editapplication">Application Details</a></li>
                                            </ul>

                                            <div class="tab-content tab-validate">
                                                <div id="Editbasic" class="tab-pane fade in active">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Student Name</label>
                                                            <input type="text" placeholder="Name" id="Editname" name="Editname" class="form-control mb" required>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Date Of Birth</label>
                                                            <input type="text" placeholder="yyyy-mm-dd" id="Editdateofbirth" name="Editdateofbirth" class="form-control mb datepicker" required>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editgenderid" name="Editgenderid" style="width: 100%;" class="select2 form-control mb select" required>
                                                                <option value="">Select Gender</option>
                                                                <?php
                                                                foreach ($genderlookup as $gender) :
                                                                ?>
                                                                    <option value="<?php echo $gender->genderid; ?>"><?php echo $gender->gendername; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editnationalityid" name="Editnationalityid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Nationality</option>
                                                                <?php
                                                                foreach ($nationalitylookup as $nationality) :
                                                                ?>
                                                                    <option value="<?php echo $nationality->nationalityid; ?>"><?php echo $nationality->nationalityname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editreligionid" name="Editreligionid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Religion</option>
                                                                <?php
                                                                foreach ($religionlookup as $religion) :
                                                                ?>
                                                                    <option value="<?php echo $religion->religionid; ?>"><?php echo $religion->religionname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editcategoryid" name="Editcategoryid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Category</option>
                                                                <?php
                                                                foreach ($categorylookup as $category) :
                                                                ?>
                                                                    <option value="<?php echo $category->categoryid; ?>"><?php echo $category->categoryname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Aadhaar Number</label>
                                                            <input type="text" placeholder="Aadhaar Number" id="Editstudentaadhaar" name="Editstudentaadhaar" class="form-control mb" required>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="Editfamily" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Father Name/ Guardian Name</label>
                                                            <input type="text" placeholder="Father Name" id="Editfathername" name="Editfathername" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Mother Name</label>
                                                            <input type="text" placeholder="Mother Name" id="Editmothername" name="Editmothername" class="form-control mb" required>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Parent Occupation</label>
                                                            <input type="text" placeholder="Parent Occupation" id="Editparentoccupation" name="Editparentoccupation" class="form-control mb" required>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div id="Editeducation" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label for="" class="text-uppercase text-sm">Which board did the student study in?</label>
                                                            <select onchange="this.classList.remove('error')" id="Editboardid" name="Editboardid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Board</option>
                                                                <?php
                                                                foreach ($boardlookup as $board) :
                                                                ?>
                                                                    <option value="<?php echo $board->boardid; ?>"><?php echo $board->boardname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">School Name</label>
                                                            <input type="text" placeholder="School Name" id="Editschoolname" name="Editschoolname" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Grade/ Marks</label>
                                                            <input type="text" placeholder="Grade/Marks" id="Editgrade" name="Editgrade" class="form-control mb" required>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Hallticket Number</label>
                                                            <input type="text" placeholder="Hallticket Number" id="Edithallticket" name="Edithallticket" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Area/ Place</label>
                                                            <input type="text" placeholder="Place of education" id="Editplaceofeducation" name="Editplaceofeducation" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Editcontact" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Door No./ Street Name</label>
                                                            <input type="text" placeholder="Door No./ Street Name" id="Editdoor_street" name="Editdoor_street" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Village/ Mandal</label>
                                                            <input type="text" placeholder="Village/ Mandal" id="Editvillage_mandal" name="Editvillage_mandal" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Landmark</label>
                                                            <input type="text" placeholder="Landmark" id="Editlandmark" name="Editlandmark" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">City/ Town</label>
                                                            <input type="text" placeholder="City/ Town" id="Editcity_town" name="Editcity_town" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">District</label>
                                                            <input type="text" placeholder="District" id="Editdistrict" name="Editdistrict" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Pin</label>
                                                            <input type="text" placeholder="Pin" id="Editpin" name="Editpin" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Mobile 1</label>
                                                            <input type="number" placeholder="Mobile 1" id="Editmobile1" name="Editmobile1" class="form-control mb" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="" class="text-uppercase text-sm">Mobile 2</label>
                                                            <input type="number" placeholder="Mobile 2" id="Editmobile2" name="Editmobile2" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label for="" class="text-uppercase text-sm">Email Address</label>
                                                            <input type="text" placeholder="Email Address" id="Editemail" name="Editemail" class="form-control mb" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Editapplication" class="tab-pane fade">
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label for="" class="text-uppercase text-sm">Application Number</label>
                                                            <input type="text" placeholder="Application Number" id="Editapplicationnumber" name="Editapplicationnumber" class="form-control" disabled required>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error');" id="Editadmissiontypeid" name="Editadmissiontypeid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Admission Type</option>
                                                                <?php
                                                                foreach ($admissiontypelookup as $admissiontype) :
                                                                ?>
                                                                    <option value="<?php echo $admissiontype->admissiontypeid; ?>"><?php echo $admissiontype->admissiontypename; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editbranchid" name="Editbranchid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Branch</option>
                                                                <?php
                                                                foreach ($branchlookup as $branch) :
                                                                ?>
                                                                    <option value="<?php echo $branch->branchid; ?>"><?php echo $branch->city . ", " . $branch->campus; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editcourseid" name="Editcourseid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Course</option>
                                                                <?php
                                                                foreach ($courselookup as $course) :
                                                                ?>
                                                                    <option value="<?php echo $course->courseid; ?>"><?php echo $course->coursename; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                            <br><br>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Edittermid" name="Edittermid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Term</option>
                                                                <?php
                                                                foreach ($termlookup as $term) :
                                                                ?>
                                                                    <option value="<?php echo $term->termid; ?>"><?php echo $term->termname; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select onchange="this.classList.remove('error')" id="Editsecondlanguageid" name="Editsecondlanguageid" style="width: 100%;" class="select2 form-control mb" required>
                                                                <option value="">Select Second Language</option>
                                                                <?php
                                                                foreach ($secondlanguagelookup as $secondlanguage) :
                                                                ?>
                                                                    <option value="<?php echo $secondlanguage->secondlanguageid; ?>"><?php echo $secondlanguage->secondlanguagename; ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <input type="hidden" id="Edituserid" name="Edituserid" />
                                            <button type="submit" id="updatestudent" name="updatestudent" class="btn btn-primary">Update Student</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include('includes/footer.php'); ?>

        <style>
            input.error {
                border-color: #f00 !important;
            }

            select.error+span>span>span {
                border-color: #f00 !important;
            }

            small.required {
                color: #f00;
            }

            .error {
                color: red;
            }
        </style>
        <!-- Loading Scripts -->
        <script>
            window.onload = function() {

                $('.datepicker').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm-dd",
                    minDate: '-20Y',
                    maxDate: '-12Y',
                });

                $('.DataTable').DataTable();
                $('.select2').select2();

                $('#studentform').validate({
                    ignore: [],
                    rules: {
                        applicationnumber: {
                            remote: "controller.php?checkapplicationnumber=1"
                        }
                    },
                    messages: {
                        applicationnumber: {
                            remote: "Application Number already exists"
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "applicationnumber" && error[0].innerHTML == "Application Number already exists") {
                            error.insertAfter("#applicationnumber");
                        }
                    },
                    submitHandler: function() {
                        form.submit();
                    },
                    invalidHandler: function() {
                        setTimeout(function() {
                            $('.nav-tabs a small.required').remove();
                            var validatePane = $('.tab-content.tab-validate .tab-pane:has(input.error)').each(function() {
                                var id = $(this).attr('id');
                                $('.nav-tabs').find('a[href^="#' + id + '"]').append(' <small class="required">*</small>');
                            });
                            var validatePaneSelect = $('.tab-content.tab-validate .tab-pane:has(select.error)').each(function() {
                                var id = $(this).attr('id');
                                $('.nav-tabs').find('a[href^="#' + id + '"]').append(' <small class="required">*</small>');
                            });
                        });
                    }
                });

                $('#editstudentform').validate({
                    ignore: [],
                    submitHandler: function() {
                        form.submit();
                    },
                    errorPlacement: function(error, element) {

                    },
                    invalidHandler: function() {
                        setTimeout(function() {
                            $('.nav-tabs a small.required').remove();
                            var validatePane = $('.tab-content.tab-validate .tab-pane:has(input.error)').each(function() {
                                var id = $(this).attr('id');
                                $('.nav-tabs').find('a[href^="#' + id + '"]').append(' <small class="required">*</small>');
                            });
                            var validatePaneSelect = $('.tab-content.tab-validate .tab-pane:has(select.error)').each(function() {
                                var id = $(this).attr('id');
                                $('.nav-tabs').find('a[href^="#' + id + '"]').append(' <small class="required">*</small>');
                            });
                        });
                    }
                });
            }

            function validateApplicationNumber(element) {
                $.ajax({
                    type: "GET",
                    url: "controller.php?checkapplicationnumber=1&applicationnumber=" + element.value,
                    success: (value) => {
                        if (value == "1") {
                            element.setCustomValidity('');
                        } else {
                            element.setCustomValidity("Applicaiton number already exists");
                        }
                    }
                });
            }

            function changefees(value) {
                if (value == 1) {
                    $('#selectadmissiontype').css("display", "none");
                    $('#dayscholarfees').css("display", "none");
                    $('#semiresidentialfees').css("display", "none");
                    $('#residentialfees').css("display", "block");
                    $('#coachingfee').val($('#rdcoaching').val());
                } else if (value == 2) {
                    $('#selectadmissiontype').css("display", "none");
                    $('#dayscholarfees').css("display", "none");
                    $('#semiresidentialfees').css("display", "block");
                    $('#residentialfees').css("display", "none");
                    $('#coachingfee').val($('#srcoaching').val());
                } else if (value == 3) {
                    $('#selectadmissiontype').css("display", "none");
                    $('#dayscholarfees').css("display", "block");
                    $('#semiresidentialfees').css("display", "none");
                    $('#residentialfees').css("display", "none");
                    $('#coachingfee').val($('#dscoaching').val());
                } else {
                    $('#selectadmissiontype').css("display", "block");
                    $('#dayscholarfees').css("display", "none");
                    $('#semiresidentialfees').css("display", "none");
                    $('#residentialfees').css("display", "none");
                    $('#coachingfee').val(0);
                }
            }

            function rdchange() {
                $('#coachingfee').val($('#rdcoaching').val());
                $('#rdtotal').text(parseFloat($('#rdcoaching').val()) + parseFloat($('#rdother').val()));
            }

            function srchange() {
                $('#coachingfee').val($('#srcoaching').val());
                $('#srtotal').text(parseFloat($('#srcoaching').val()) + parseFloat($('#srother').val()));
            }

            function dschange() {
                $('#coachingfee').val($('#dscoaching').val());
                $('#dstotal').text(parseFloat($('#dscoaching').val()) + parseFloat($('#dsother').val()));
            }
        </script>
    </body>

    </html>
<?php } ?>