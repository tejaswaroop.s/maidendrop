<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

$pageaccess = $_SESSION['rights'][array_search('Approval', array_column($_SESSION['rights'], 'operationname'))];
if ($pageaccess->_view != 1) {
    header('location:index.php');
} else {
?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>
            <div class="content-wrapper">
                <div class="container-fluid">
                    <?php if ($_SESSION['rights'][array_search('Approval', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="page-title">Student Approvals</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="tblApprovals" class="DataTable table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Student Name</th>
                                                    <th>Current Status</th>
                                                    <th>Branch - Cource - Term - Admission Type</th>
                                                    <th>Approval Amount</th>
                                                    <th>Comment</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($_SESSION['userdetails']->branchid == NULL) {
                                                    $sql = "SELECT * from users 
                                                    JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                    JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                    JOIN courselookup ON users.courseid = courselookup.courseid
                                                    JOIN termlookup ON users.termid = termlookup.termid
                                                    JOIN applicationstatuslookup ON users.applicationstatusid = applicationstatuslookup.applicationstatusid
                                                    WHERE users.applicationstatusid <> 2 and users.applicationstatusid <> 4";
                                                    // WHERE sendapprovalto = 'SA' and users.applicationstatusid <> 2 and users.applicationstatusid <> 4

                                                } else {
                                                    $sql = "SELECT * from users 
                                                    JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                    JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                    JOIN courselookup ON users.courseid = courselookup.courseid
                                                    JOIN termlookup ON users.termid = termlookup.termid
                                                    JOIN applicationstatuslookup ON users.applicationstatusid = applicationstatuslookup.applicationstatusid
                                                    WHERE users.applicationstatusid <> 2 and users.applicationstatusid <> 4 and branchlookup.branchid IN ({$_SESSION['userdetails']->branchid})";
                                                    // WHERE sendapprovalto = 'CH' and users.applicationstatusid <> 2 and users.applicationstatusid <> 4 and branchlookup.branchid = {$_SESSION['userdetails']->branchid}

                                                }
                                                $query = $dbh->prepare($sql);
                                                $query->execute();
                                                $results = $query->fetchAll(PDO::FETCH_OBJ);

                                                foreach ($results as $result) :
                                                ?>
                                                    <tr>
                                                        <td><?php echo $result->name ?></td>
                                                        <td><?php echo $result->applicationstatusname ?></td>
                                                        <td><?php echo $result->city . ", " . $result->campus ?><br />
                                                            <?php echo $result->coursename . ", " . $result->termname ?><br />
                                                            <?php echo $result->admissiontypename ?></td>
                                                        <?php

                                                        if ($_SESSION['userdetails']->branchid != NULL && $result->sendapprovalto == "SA") {
                                                        ?>
                                                            <td></td>
                                                            <td>Pending Approval with SA</td>
                                                            <td></td>

                                                        <?php
                                                        } else {
                                                        ?>
                                                            <td><input type="number" placeholder="coachingfee" id="<?php echo $result->userid ?>coachingfee" value="<?php echo $result->coachingfee ?>" class="form-control mb"><br>
                                                                <a class="btn btn-warning" onclick="updatestudent('<?php echo $result->userid ?>')">Update</a><br><br>
                                                            </td>
                                                            <td><input type="text" placeholder="comments" id="<?php echo $result->userid ?>comments" value="<?php echo $result->comments ?>" class="form-control mb"></td>
                                                            <td><a class="btn btn-success" onclick="approvestudent('<?php echo $result->userid ?>')">Approve</a><br><br>
                                                                <a class="btn btn-danger" onclick="declinestudent('<?php echo $result->userid ?>')">Decline</a></td>
                                                        <?php
                                                        }

                                                        ?>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Declined Appications</h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tbldeclined" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Student Name</th>
                                                <th>Branch - Cource - Term - Admission Type</th>
                                                <th>Comment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($_SESSION['userdetails']->branchid == NULL) {
                                                $sql = "SELECT * from users 
                                                                 JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                                 JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                                 JOIN courselookup ON users.courseid = courselookup.courseid
                                                                 JOIN termlookup ON users.termid = termlookup.termid
                                                                 WHERE sendapprovalto = 'SA' and applicationstatusid = 2";
                                            } else {
                                                $sql = "SELECT * from users 
                                                                 JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                                 JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                                 JOIN courselookup ON users.courseid = courselookup.courseid
                                                                 JOIN termlookup ON users.termid = termlookup.termid
                                                                 WHERE sendapprovalto = 'CH' and applicationstatusid = 2 and branchlookup.branchid IN ({$_SESSION['userdetails']->branchid})";
                                            }

                                            $query = $dbh->prepare($sql);
                                            $query->execute();
                                            $results = $query->fetchAll(PDO::FETCH_OBJ);

                                            foreach ($results as $result) :
                                            ?>
                                                <tr>
                                                    <td><?php echo $result->name ?></td>
                                                    <td><?php echo $result->city . ", " . $result->campus ?><br />
                                                        <?php echo $result->coursename . ", " . $result->termname ?><br />
                                                        <?php echo $result->admissiontypename ?></td>
                                                    <td><?php echo $result->comments ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        </div>

        <!-- Loading Scripts -->
        <script>
            window.onload = function() {
                $('.DataTable').DataTable();
            }

            function updatestudent(userid) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        updatestudent: true,
                        userid: userid,
                        coachingfee: $('#' + userid + "coachingfee").val(),
                        comments: $('#' + userid + "comments").val()
                    },
                    success: () => {
                        location.reload();
                    }
                });
            }

            function approvestudent(userid) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        approvestudent: true,
                        userid: userid,
                        comments: $('#' + userid + "comments").val()
                    },
                    success: () => {
                        location.reload();
                    }
                });
            }

            function declinestudent(userid) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        declinestudent: true,
                        userid: userid,
                        comments: $('#' + userid + "comments").val()
                    },
                    success: () => {
                        location.reload();
                    }
                });

            }
        </script>
    </body>

    </html>
<?php } ?>