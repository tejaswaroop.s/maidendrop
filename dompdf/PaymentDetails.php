<?php
namespace Dompdf;
require_once 'dompdf/autoload.inc.php';
include('../includes/config.php');

$paymentid = $_GET['paymentid'];
$outputType = $_GET['outputType'];

$serverURI = $_SERVER['SERVER_NAME'];
$html = file_get_contents("http://{$serverURI}{$foldername}/includes/generatereceipt.php?paymentid={$paymentid}");
$dompdf = new Dompdf();
$dompdf->setPaper('A4', 'landscape');
$dompdf->loadHtml($html);
$dompdf->render();

$pdf_name = $paymentid;

switch ($outputType) {
  case "View":
    $dompdf->stream("", array("Attachment" => false));
    break;

  case "Save":
    $output = $dompdf->output();
    file_put_contents("payment_files/{$pdf_name}.pdf", $output);
    break;

  case "Download":
    $dompdf->stream("{$pdf_name}");
    break;
}