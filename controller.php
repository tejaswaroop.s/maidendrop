<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (isset($_GET['checkapplicationnumber'])) {
    $sql = "SELECT * from users WHERE applicationnumber='" . $_GET['applicationnumber'] . "'";
    $query = $dbh->prepare($sql);
    $query->execute();
    $application = $query->fetchAll(PDO::FETCH_OBJ);
    if (COUNT($application) == 0) {
        echo "true";
    } else {
        echo "false";
    }
}

if (isset($_GET['Autocomplete'])) {
    $searchfor = $_GET['term'];

    if ($_SESSION['userdetails']->branchid == NULL) {
        $sql = "SELECT * FROM users JOIN roleslookup ON users.roleid = roleslookup.roleid 
                         WHERE users.name like '%$searchfor%' OR
                            users.mobile1 like '%$searchfor%' OR
                            users.mobile2 like '%$searchfor%' OR
                            users.email like '%$searchfor%' OR
                            users.applicationnumber like '%$searchfor%' LIMIT 5";
    } else {
        $sql = "SELECT * FROM users JOIN roleslookup ON users.roleid = roleslookup.roleid 
                         WHERE (users.name like '%$searchfor%' OR
                                users.mobile1 like '%$searchfor%' OR
                                users.mobile2 like '%$searchfor%' OR
                                users.email like '%$searchfor%' OR
                                users.applicationnumber like '%$searchfor%') AND users.branchid IN ({$_SESSION['userdetails']->branchid}) LIMIT 5 ";
    }


    $query = $dbh->prepare($sql);
    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->execute();

    $userdetails = $query->fetchAll(PDO::FETCH_OBJ);
    $userData = array();

    foreach ($userdetails as $user) {
        if ($user->rolename == "Student") {
            $imageURL = $user->image != 1 ? "images/user-image.png" : "images/{$user->userid}.png";
            $data['id']    = $user->userid;
            $data['value'] = $user->name;
            $data['label'] = '<a href="javascript:void(0);' . $user->userid . '">
                <img src="' . $imageURL . '" width="50" height="50"/>
                <span>' . $user->applicationnumber . ' - ' . $user->name . '</span><br />
                <small>' . $user->email . ', ' . $user->mobile1 . ', ' . $user->mobile2 . '</small>
            </a>';
            array_push($userData, $data);
        }
    }
    echo json_encode($userData);
}

if (isset($_POST['updatestudent'])) {
    $coachingfee = $_POST['coachingfee'];
    $comments = $_POST['comments'];
    $userid = $_POST['userid'];
    $applicationstatusid = 3;

    $sql = "UPDATE users SET coachingfee=:coachingfee, comments=:comments, applicationstatusid=:applicationstatusid WHERE userid=:userid";

    $query = $dbh->prepare($sql);
    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->bindParam(':coachingfee', $coachingfee, PDO::PARAM_STR);
    $query->bindParam(':applicationstatusid', $applicationstatusid, PDO::PARAM_STR);
    $query->bindParam(':comments', $comments, PDO::PARAM_STR);
    $query->execute();
}

if (isset($_POST['approvestudent'])) {
    $userid = $_POST['userid'];
    $comments = $_POST['comments'];
    $applicationstatusid = 4;

    $sql = "UPDATE users SET comments=:comments, applicationstatusid=:applicationstatusid WHERE userid=:userid";

    $query = $dbh->prepare($sql);
    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->bindParam(':applicationstatusid', $applicationstatusid, PDO::PARAM_STR);
    $query->bindParam(':comments', $comments, PDO::PARAM_STR);
    $query->execute();

    $sql = "SELECT * from users WHERE userid = {$userid}";
    $query = $dbh->prepare($sql);
    $query->execute();
    $users = $query->fetchAll(PDO::FETCH_OBJ);

    $invoiceid = "RHYD-" . "{$users[0]->applicationnumber}-1";

    foreach ($feestructurelookup as $feestructure) {
        if ($feestructure->admissiontypeid == $users[0]->admissiontypeid) {
            if ($feestructure->canchange == 1) {
                $sql = "INSERT INTO invoices (invoiceid, userid, feesid, feesvalue) 
                              VALUES (:invoiceid,:userid,:feesid,:feesvalue)";

                $query = $dbh->prepare($sql);
                $query->bindParam(':invoiceid', $invoiceid, PDO::PARAM_STR);
                $query->bindParam(':userid', $userid, PDO::PARAM_STR);
                $query->bindParam(':feesid', $feestructure->feesid, PDO::PARAM_STR);
                $query->bindParam(':feesvalue', $users[0]->coachingfee, PDO::PARAM_STR);
                $query->execute();
            } else {
                $sql = "INSERT INTO invoices (invoiceid, userid, feesid, feesvalue) 
                              VALUES (:invoiceid,:userid,:feesid,:feesvalue)";

                $query = $dbh->prepare($sql);
                $query->bindParam(':invoiceid', $invoiceid, PDO::PARAM_STR);
                $query->bindParam(':userid', $userid, PDO::PARAM_STR);
                $query->bindParam(':feesid', $feestructure->feesid, PDO::PARAM_STR);
                $query->bindParam(':feesvalue', $feestructure->feesvalue, PDO::PARAM_STR);
                $query->execute();
            }
        }
    }

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, "http://localhost/maidendrop/dompdf/InvoiceDetails.php?invoiceid=RHYD-MD01978-1&outputType=Save");
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output = curl_exec($ch);
    // curl_close($ch);

    $serverURI = $_SERVER['SERVER_NAME'];
    $html = file_get_contents("http://{$serverURI}{$foldername}/dompdf/InvoiceDetails.php?invoiceid={$invoiceid}&outputType=Save");

    $docROOT = $_SERVER["DOCUMENT_ROOT"];
    $filename = "{$invoiceid}.pdf";
    $path = "{$docROOT}{$foldername}/dompdf/invoices_files/";
    $file = $path . $filename;

    $email = $users[0]->email;
    $subject = 'Maidendrop Group - Invoice Details';
    $message = 'Thank you for joining maidendrop group. Your Invoice details are attached as a document. Thank You';

    $content = file_get_contents($file);
    $content = chunk_split(base64_encode($content));

    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // carriage return type (RFC)
    $eol = "\r\n";

    // main header (multipart mandatory)
    $headers = "From: Maidendropgroup <admin@maidendropgroup.com>" . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
    $headers .= "This is a MIME encoded message." . $eol;

    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol;
    $body .= $message . $eol;

    // attachment
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol;
    $body .= $content . $eol;
    $body .= "--" . $separator . "--";

    //SEND Mail
    if (isset($mailto)) {
        mail($mailto, $subject, $body, $headers);
    }
}

if (isset($_GET['getstudent'])) {
    $userid = $_GET['userid'];

    $sql = "SELECT * FROM users WHERE userid=:userid";

    $query = $dbh->prepare($sql);
    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->execute();

    $userdetails = $query->fetchAll(PDO::FETCH_OBJ);
    echo json_encode($userdetails[0]);
}

if (isset($_POST['declinestudent'])) {
    $userid = $_POST['userid'];
    $comments = $_POST['comments'];
    $applicationstatusid = 2;

    $sql = "UPDATE users SET comments=:comments, applicationstatusid=:applicationstatusid WHERE userid=:userid";

    $query = $dbh->prepare($sql);
    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->bindParam(':applicationstatusid', $applicationstatusid, PDO::PARAM_STR);
    $query->bindParam(':comments', $comments, PDO::PARAM_STR);
    $query->execute();
}

if (isset($_POST['approvepayment'])) {
    $paymentid = $_POST['paymentid'];
    $paymentstatusid = 3;

    $sql = "UPDATE payments SET paymentstatusid=:paymentstatusid WHERE paymentid=:paymentid";

    $query = $dbh->prepare($sql);
    $query->bindParam(':paymentstatusid', $paymentstatusid, PDO::PARAM_STR);
    $query->bindParam(':paymentid', $paymentid, PDO::PARAM_STR);
    $query->execute();


    $sql = "SELECT * FROM payments WHERE paymentid=:paymentid";
    $query = $dbh->prepare($sql);
    $query->bindParam(':paymentid', $paymentid, PDO::PARAM_STR);
    $query->execute();
    $paymentDetails = $query->fetchAll(PDO::FETCH_OBJ);

    $userid = $paymentDetails[0]->userid;
    $sql = "SELECT * FROM users WHERE userid=:userid";
    $query = $dbh->prepare($sql);
    $query->bindParam(':userid', $userid, PDO::PARAM_STR);
    $query->execute();
    $userDetails = $query->fetchAll(PDO::FETCH_OBJ);

    $serverURI = $_SERVER['SERVER_NAME'];
    $html = file_get_contents("http://{$serverURI}{$foldername}/dompdf/PaymentDetails.php?paymentid={$paymentid}&outputType=Save");

    $docROOT = $_SERVER["DOCUMENT_ROOT"];
    $filename = "{$paymentid}.pdf";
    $path = "{$docROOT}{$foldername}/dompdf/payment_files/";
    $file = $path . $filename;

    $email = $userDetails[0]->email;
    $subject = 'Maidendrop Group - Payment Details';
    $message = 'Thank you for your payment. Your Payment details are attached as a document. Thank You';

    $content = file_get_contents($file);
    $content = chunk_split(base64_encode($content));

    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // carriage return type (RFC)
    $eol = "\r\n";

    // main header (multipart mandatory)
    $headers = "From: Maidendropgroup <admin@maidendropgroup.com>" . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
    $headers .= "This is a MIME encoded message." . $eol;

    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol;
    $body .= $message . $eol;

    // attachment
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol;
    $body .= $content . $eol;
    $body .= "--" . $separator . "--";

    //SEND Mail
    if (isset($mailto)) {
        mail($mailto, $subject, $body, $headers);
    }
}

if (isset($_POST['sendinvoiceemail'])) {
    $invoiceid = $_POST['invoiceid'];
    $emailaddress = $_POST['emailaddress'];

    $serverURI = $_SERVER['SERVER_NAME'];
    $html = file_get_contents("http://{$serverURI}{$foldername}/dompdf/InvoiceDetails.php?invoiceid={$invoiceid}&outputType=Save");

    $docROOT = $_SERVER["DOCUMENT_ROOT"];
    $filename = "{$invoiceid}.pdf";
    $path = "{$docROOT}{$foldername}/dompdf/invoices_files/";
    $file = $path . $filename;

    $email = $emailaddress;
    $subject = 'Maidendrop Group - Invoice Details';
    $message = 'Thank you for joining maidendrop group. Your Invoice details are attached as a document. Thank You';

    $content = file_get_contents($file);
    $content = chunk_split(base64_encode($content));

    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // carriage return type (RFC)
    $eol = "\r\n";

    // main header (multipart mandatory)
    $headers = "From: Maidendropgroup <admin@maidendropgroup.com>" . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
    $headers .= "This is a MIME encoded message." . $eol;

    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol;
    $body .= $message . $eol;

    // attachment
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol;
    $body .= $content . $eol;
    $body .= "--" . $separator . "--";

    //SEND Mail
    if (isset($mailto)) {
        mail($mailto, $subject, $body, $headers);
    }
}

if (isset($_POST['sendpaymentemail'])) {
    $paymentid = $_POST['paymentid'];
    $emailaddress = $_POST['emailaddress'];

    $serverURI = $_SERVER['SERVER_NAME'];
    $html = file_get_contents("http://{$serverURI}{$foldername}/dompdf/PaymentDetails.php?paymentid={$paymentid}&outputType=Save");

    $docROOT = $_SERVER["DOCUMENT_ROOT"];
    $filename = "{$paymentid}.pdf";
    $path = "{$docROOT}{$foldername}/dompdf/payment_files/";
    $file = $path . $filename;

    $email = $emailaddress;
    $subject = 'Maidendrop Group - Payment Details';
    $message = 'Thank you for your payment. Your Payment details are attached as a document. Thank You';

    $content = file_get_contents($file);
    $content = chunk_split(base64_encode($content));

    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // carriage return type (RFC)
    $eol = "\r\n";

    // main header (multipart mandatory)
    $headers = "From: Maidendropgroup <admin@maidendropgroup.com>" . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
    $headers .= "This is a MIME encoded message." . $eol;

    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol;
    $body .= $message . $eol;

    // attachment
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol;
    $body .= $content . $eol;
    $body .= "--" . $separator . "--";

    //SEND Mail
    if (isset($mailto)) {
        mail($mailto, $subject, $body, $headers);
    }
}

if (isset($_GET['testemail'])) {
    $to = "stvstejaswaroop@gmail.com";
    $subject = 'MAIDENDROPGROUP';
    $message = 'Dear Student Your Payment has been Approved.';
    $headers = 'From: admin@maidendropgroup.com' . "\r\n" .
        'Reply-To: admin@maidendropgroup.com' . "\r\n" .
        'MIME-Version: 1.0' . "\r\n" .
        'Content-Type: text/html; charset=UTF-8' . "\r\n";

    mail($to, $subject, $message, $headers);
}

if (isset($_POST['rejectpayment'])) {
    $paymentid = $_POST['paymentid'];
    $paymentstatusid = 2;

    $sql = "UPDATE payments SET paymentstatusid=:paymentstatusid WHERE paymentid=:paymentid";

    $query = $dbh->prepare($sql);
    $query->bindParam(':paymentstatusid', $paymentstatusid, PDO::PARAM_STR);
    $query->bindParam(':paymentid', $paymentid, PDO::PARAM_STR);
    $query->execute();
}

if (isset($_POST['setright'])) {
    $operationid = $_POST['operationid'];
    $right = $_POST['right'];
    $roleid = $_POST['roleid'];
    $checked = $_POST['checked'] == "true" ? 1 : 0;

    switch ($right) {
        case "add":
            $sql = "UPDATE rights set _add = {$checked} WHERE roleid={$roleid} and operationid={$operationid}";
            break;
        case "view":
            $sql = "UPDATE rights set _view = {$checked} WHERE roleid={$roleid} and operationid={$operationid}";
            break;
        case "edit":
            $sql = "UPDATE rights set _edit = {$checked} WHERE roleid={$roleid} and operationid={$operationid}";
            break;
        case "delete":
            $sql = "UPDATE rights set _delete = {$checked} WHERE roleid={$roleid} and operationid={$operationid}";
            break;
        default:
    }
    $query = $dbh->prepare($sql);
    $query->execute();
}
