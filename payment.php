<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');
include('includes/commonfunctions.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

$pageaccess = $_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))];
if ($pageaccess->_view != 1) {
    header('location:index.php');
} else {

    if (isset($_POST['addpayment'])) {
        $userid = $_POST['userid'];
        $paymenttypeid = $_POST['paymenttypeid'];
        $paymentamount = $_POST['paymentamount'];
        $paymentdate = date('Y-m-d');
        $otherdetails = $_POST['otherdetails'];
        $paymentcollectedby = $_SESSION['userdetails']->userid;
        $paymentstatusid = 1;

        $batch = get_batch();
        $nextpaymentid = get_paymentidcounter();
        $paymentid = "RMD-" . $batch . "-" . str_pad($nextpaymentid, 6, '0', STR_PAD_LEFT);

        $sql = "INSERT INTO payments(paymentid, userid, paymentamount, paymentdate, paymenttypeid, otherdetails, paymentcollectedby, paymentstatusid) 
                             VALUES(:paymentid,:userid,:paymentamount,:paymentdate,:paymenttypeid,:otherdetails,:paymentcollectedby,:paymentstatusid)";
        $query = $dbh->prepare($sql);
        $query->bindParam(':paymentid', $paymentid, PDO::PARAM_STR);
        $query->bindParam(':userid', $userid, PDO::PARAM_STR);
        $query->bindParam(':paymentamount', $paymentamount, PDO::PARAM_STR);
        $query->bindParam(':paymentdate', $paymentdate, PDO::PARAM_STR);
        $query->bindParam(':paymenttypeid', $paymenttypeid, PDO::PARAM_STR);
        $query->bindParam(':otherdetails', $otherdetails, PDO::PARAM_STR);
        $query->bindParam(':paymentcollectedby', $paymentcollectedby, PDO::PARAM_STR);
        $query->bindParam(':paymentstatusid', $paymentstatusid, PDO::PARAM_STR);
        $issuccess = $query->execute();

        if ($issuccess) {
            set_paymentidcounter();
        }

        header('location:payment.php');
    }
?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>
            <div class="content-wrapper">
                <div class="container-fluid">

                    <?php if ($_SESSION['rights'][array_search('PaymentApproval', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>

                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="page-title">Payment Approvals</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="tblpaymentApproval" class="DataTable table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Payment Id</th>
                                                    <th>Application Number</th>
                                                    <th>Name</th>
                                                    <th>Payment Status</th>
                                                    <th>Payment</th>
                                                    <th>Payment Date</th>
                                                    <th>Payment Type</th>
                                                    <th>Payment Details</th>
                                                    <th>Amount Received By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                $sql = "SELECT U.applicationnumber, U.name, PSL.paymentstatusname, paymentid, paymentamount, paymentdate, paymenttypename, otherdetails, PC.name as receivedby 
                                                    FROM payments P 
                                                    JOIN users U ON P.userid = U.userid 
                                                    JOIN paymentstatuslookup PSL ON P.paymentstatusid = PSL.paymentstatusid 
                                                    JOIN paymenttypelookup PT ON P.paymenttypeid = PT.paymenttypeid 
                                                    LEFT JOIN users PC ON P.paymentcollectedby = PC.userid
                                                    WHERE PSL.paymentstatusid = 1";
                                                $query = $dbh->prepare($sql);
                                                $query->execute();
                                                $results = $query->fetchAll(PDO::FETCH_OBJ);

                                                foreach ($results as $result) :
                                                ?>
                                                    <tr>
                                                        <td><?php echo $result->paymentid ?></td>
                                                        <td><?php echo $result->applicationnumber ?></td>
                                                        <td><?php echo $result->name ?></td>
                                                        <td><?php echo $result->paymentstatusname ?></td>
                                                        <td><?php echo $result->paymentamount ?></td>
                                                        <td><?php echo $result->paymentdate ?></td>
                                                        <td><?php echo $result->paymenttypename ?></td>
                                                        <td><?php echo $result->otherdetails ?></td>
                                                        <td><?php echo $result->receivedby ?></td>
                                                        <td><a class="btn btn-success" onclick="approvePayment('<?php echo $result->paymentid ?>')">Approve</a><br><br>
                                                            <a class="btn btn-danger" onclick="rejectPayment('<?php echo $result->paymentid ?>')">Reject</a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Payments
                                <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_add == 1) : ?>
                                    <a class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#addpayment">Add Payment</a>
                                <?php endif; ?>
                            </h2>
                            <form action="" method="POST">
                                <div class="form-group row">
                                    <div class="col-xs-2">
                                        <label for="ex1">Payment Status</label>
                                        <select name="filter_paymentstatus" class="form-control">
                                            <option value="">Select Status</option>
                                            <?php
                                            foreach ($paymentstatuslookup as $payment) :
                                            ?>
                                                <option value="<?php echo $payment->paymentstatusname; ?>"><?php echo $payment->paymentstatusname; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-xs-2">
                                        <label for="ex1">Recieved By</label>
                                        <select name="filter_amountrecievedby" class="form-control">
                                            <option value="">Select Received By</option>
                                            <?php
                                            foreach ($userslookup as $users) :
                                            ?>
                                                <option value="<?php echo $users->userid; ?>"><?php echo $users->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="ex1">Payment Amount</label>
                                        <select name="payment_amount" class="form-control">
                                            <option value="">Select Amount</option>
                                            <option value="OR paymentamount BETWEEN 0 AND 20000">&#8377;0 - &#8377;20,000/-</option>
                                            <option value="OR paymentamount BETWEEN 20000 AND 40000">&#8377;20,000 - &#8377;40,000/-</option>
                                            <option value="OR paymentamount BETWEEN 40000 AND 60000">&#8377;40,000 - &#8377;60,000/-</option>
                                            <option value="OR paymentamount BETWEEN 60000 AND 80000">&#8377;60,000 - &#8377;80,000/-</option>
                                            <option value="OR paymentamount BETWEEN 80000 AND 100000">&#8377;80,000 - &#8377;1,00,000/-</option>
                                            <option value="OR paymentamount BETWEEN 100000 AND 120000">&#8377;1,00,000 - &#8377;1,20,000/-</option>
                                            <option value="OR paymentamount BETWEEN 120000 AND 140000">&#8377;1,20,000 - &#8377;1,40,000/-</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="ex1">Term</label>
                                        <select name="filter_term" class="form-control">
                                            <option value="">Select Term</option>
                                            <?php
                                            foreach ($termlookup as $term) :
                                            ?>
                                                <option value="<?php echo $term->termid; ?>"><?php echo $term->termname; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="ex1">Date From</label>
                                        <input type="date" name="filter_from_date" class="form-control">
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="ex1">Date To</label>
                                        <input type="date" name="filter_to_date" class="form-control">
                                    </div>



                                    <div class="col-xs-2">
                                        <br>
                                        <button type="submit" name="filter" id="filter" class="btn btn-info">Filter</button>
                                    </div>

                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tblPayments" class="DataTable table table-striped">
                                        <thead>
                                            <tr>
                                                <th>PaymentId</th>
                                                <th>Name</th>
                                                <th>Payment Status</th>
                                                <th>Payment</th>
                                                <th>Payment Date</th>
                                                <th>Payment Type</th>
                                                <th>Payment Details</th>
                                                <th>Amount Received By</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($_SESSION['userdetails']->branchid == NULL) {
                                                $filter_paymentstatus = isset($_POST['filter_paymentstatus']) ? $_POST['filter_paymentstatus'] : "";
                                                $filter_amountrecievedby = isset($_POST['filter_amountrecievedby']) ? $_POST['filter_amountrecievedby'] : "";
                                                $payment_amount = isset($_POST['payment_amount']) ? $_POST['payment_amount'] : "";
                                                $filter_term = isset($_POST['filter_term']) ? $_POST['filter_term'] : "";
                                                $filter_from_date = isset($_POST['filter_from_date']) ? $_POST['filter_from_date'] : "";
                                                $filter_to_date = isset($_POST['filter_to_date']) ? $_POST['filter_to_date'] : "";

                                                $sql =
                                                    $sql =
                                                    "SELECT U.applicationnumber, U.name, PSL.paymentstatusname, paymentid, U.email, paymentamount, paymentdate, paymenttypename, otherdetails, PC.name as receivedby 
                                                     FROM payments P 
                                                     JOIN users U ON P.userid = U.userid 
                                                     JOIN paymentstatuslookup PSL ON P.paymentstatusid = PSL.paymentstatusid 
                                                     JOIN paymenttypelookup PT ON P.paymenttypeid = PT.paymenttypeid 
                                                     LEFT JOIN users PC ON P.paymentcollectedby = PC.userid WHERE paymentstatusname='$filter_paymentstatus' OR paymentcollectedby='$filter_amountrecievedby' $payment_amount OR paymentdate BETWEEN '$filter_from_date' AND '$filter_to_date' OR U.termid='$filter_term'";
                                            } else {

                                                $filter_paymentstatus = isset($_POST['filter_paymentstatus']) ? $_POST['filter_paymentstatus'] : "";
                                                $filter_amountrecievedby = isset($_POST['filter_amountrecievedby']) ? $_POST['filter_to_date'] : "";
                                                $payment_amount = isset($_POST['payment_amount']) ? $_POST['payment_amount'] : "";
                                                $filter_term = isset($_POST['filter_term']) ? $_POST['filter_term'] : "";
                                                $filter_from_date = isset($_POST['filter_from_date']) ? $_POST['filter_from_date'] : "";
                                                $filter_to_date = isset($_POST['filter_to_date']) ? $_POST['filter_to_date'] : "";

                                                $sql =
                                                    "SELECT U.applicationnumber, U.name, PSL.paymentstatusname, paymentid, U.email, paymentamount, paymentdate, paymenttypename, otherdetails, PC.name as receivedby 
                                                     FROM payments P 
                                                     JOIN users U ON P.userid = U.userid 
                                                     JOIN paymentstatuslookup PSL ON P.paymentstatusid = PSL.paymentstatusid 
                                                     JOIN paymenttypelookup PT ON P.paymenttypeid = PT.paymenttypeid 
                                                     LEFT JOIN users PC ON P.paymentcollectedby = PC.userid
                                                     WHERE U.branchid IN ({$_SESSION['userdetails']->branchid}) AND paymentstatusname='$filter_paymentstatus' OR paymentcollectedby='$filter_amountrecievedby' $payment_amount OR paymentdate BETWEEN '$filter_from_date' AND '$filter_to_date' OR U.termid='$filter_term'";
                                            }

                                            $query = $dbh->prepare($sql);
                                            $query->execute();
                                            $results = $query->fetchAll(PDO::FETCH_OBJ);

                                            foreach ($results as $result) :
                                            ?>
                                                <tr>
                                                    <td><a target="_blank" href="dompdf/PaymentDetails.php?paymentid=<?php echo $result->paymentid ?>&outputType=View"><?php echo $result->paymentid ?></a></td>
                                                    <td><?php echo $result->name ?></td>
                                                    <td><?php echo $result->paymentstatusname ?></td>
                                                    <td><?php echo $result->paymentamount ?></td>
                                                    <td><?php echo $result->paymentdate ?></td>
                                                    <td><?php echo $result->paymenttypename ?></td>
                                                    <td><?php echo $result->otherdetails ?></td>
                                                    <td><?php echo $result->receivedby ?></td>
                                                    <td>
                                                        <a title="View" target="_blank" href="dompdf/PaymentDetails.php?paymentid=<?php echo $result->paymentid ?>&outputType=View"><i class="fa fa-eye"></i></a>&nbsp;
                                                        <a title="Download" href="dompdf/PaymentDetails.php?paymentid=<?php echo $result->paymentid ?>&outputType=Download"><i class="fa fa-download"></i></a>&nbsp;
                                                        <a title="Send Email" onclick="sendpaymentemail('<?php echo $result->paymentid ?>', '<?php echo $result->email ?>')"><i class="fa fa-paper-plane"></i></a>
                                                        <br /><br />
                                                        <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_edit == 1) : ?>
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        <?php endif; ?>
                                                        &nbsp;
                                                        <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_delete == 1) : ?>
                                                            <i class="fa fa-trash"></i>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="addpayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Add Payment</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <select name="userid" style="width: 100%;" class="select2 form-control mb" required>
                                                        <option value="">Select Student</option>
                                                        <?php

                                                        if ($_SESSION['userdetails']->branchid == NULL) {
                                                            $sql = "SELECT * from users WHERE roleid = 5";
                                                        } else {
                                                            $sql = "SELECT * from users WHERE roleid = 5 and users.branchid IN ({$_SESSION['userdetails']->branchid})";
                                                        }
                                                        $query = $dbh->prepare($sql);
                                                        $query->execute();
                                                        $studentdetails = $query->fetchAll(PDO::FETCH_OBJ);

                                                        foreach ($studentdetails as $student) :
                                                        ?>
                                                            <option value="<?php echo $student->userid; ?>"><?php echo $student->applicationnumber . " - " .  $student->name; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                    <br><br>
                                                </div>
                                                <div class="col-md-6">
                                                    <select name="paymenttypeid" style="width: 100%;" class="select2 form-control mb" required>
                                                        <option value="">Select Payment Type</option>
                                                        <?php

                                                        foreach ($paymenttypelookup as $paymenttype) :
                                                        ?>
                                                            <option value="<?php echo $paymenttype->paymenttypeid; ?>"><?php echo $paymenttype->paymenttypename; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Payment Amount</label>
                                                    <input type="number" placeholder="Payment Amount" name="paymentamount" class="form-control mb" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="" class="text-uppercase text-sm">Payment Details</label>
                                                    <input type="text" placeholder="UTR No / Check No / Recipt No" name="otherdetails" class="form-control mb" required>
                                                </div>
                                            </div>
                                            <br />
                                            <button type="submit" name="addpayment" class="btn btn-primary">Add Payment</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <!-- Loading Scripts -->
        <script>
            window.onload = function() {
                $('.DataTable').DataTable();
                $('.select2').select2();
                //$('#tblPayments').ddTableFilter();
            }

            function approvePayment(paymentid) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        approvepayment: true,
                        paymentid: paymentid,
                    },
                    success: () => {
                        location.reload();
                    }
                });
            }

            function rejectPayment(paymentid) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        rejectpayment: true,
                        paymentid: paymentid,
                    },
                    success: () => {
                        location.reload();
                    }
                });

            }

            function sendpaymentemail(paymentid, emailaddress) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        sendpaymentemail: true,
                        paymentid: paymentid,
                        emailaddress: emailaddress
                    },
                    success: () => {
                        alert('Email Sent Successfully');
                    }
                });
            }
        </script>
    </body>

    </html>
<?php } ?>