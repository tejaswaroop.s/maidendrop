<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');
include('includes/commonfunctions.php');

if (COUNT($_SESSION) == 0) {
   header('location:index.php');
}

$pageaccess = $_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))];
if ($pageaccess->_view != 1) {
   header('location:index.php');
} else {

   if (isset($_POST['addpayment'])) {
      if (isset($_GET['student_id'])) {
         $student_id = $_GET['student_id'];

         $userid = $_POST['userid'];
         $paymenttypeid = $_POST['paymenttypeid'];
         $paymentamount = $_POST['paymentamount'];
         $paymentdate = date('Y-m-d');
         $otherdetails = $_POST['otherdetails'];
         $paymentcollectedby = $_SESSION['userdetails']->userid;
         $paymentstatusid = 1;

         $batch = get_batch();
         $nextpaymentid = get_paymentidcounter();
         $paymentid = "RMD-" . $batch . "-" . str_pad($nextpaymentid, 6, '0', STR_PAD_LEFT);

         $sql = "INSERT INTO payments(paymentid, userid, paymentamount, paymentdate, paymenttypeid, otherdetails, paymentcollectedby, paymentstatusid) 
                             VALUES(:paymentid,:userid,:paymentamount,:paymentdate,:paymenttypeid,:otherdetails,:paymentcollectedby,:paymentstatusid)";
         $query = $dbh->prepare($sql);
         $query->bindParam(':paymentid', $paymentid, PDO::PARAM_STR);
         $query->bindParam(':userid', $userid, PDO::PARAM_STR);
         $query->bindParam(':paymentamount', $paymentamount, PDO::PARAM_STR);
         $query->bindParam(':paymentdate', $paymentdate, PDO::PARAM_STR);
         $query->bindParam(':paymenttypeid', $paymenttypeid, PDO::PARAM_STR);
         $query->bindParam(':otherdetails', $otherdetails, PDO::PARAM_STR);
         $query->bindParam(':paymentcollectedby', $paymentcollectedby, PDO::PARAM_STR);
         $query->bindParam(':paymentstatusid', $paymentstatusid, PDO::PARAM_STR);
         $issuccess = $query->execute();

         if ($issuccess) {
            set_paymentidcounter();
         }

         header("location:student_details.php?student_id={$student_id}");
      }
   }

   if (isset($_POST['updatestudent'])) {
      $userid = $_POST['Edituserid'];

      $name = $_POST['Editname'];
      $fathername = $_POST['Editfathername'];
      $mothername = $_POST['Editmothername'];
      $parentoccupation = $_POST['Editparentoccupation'];
      $dateofbirth = $_POST['Editdateofbirth'];
      $genderid = $_POST['Editgenderid'];
      $studentaadhaar = $_POST['Editstudentaadhaar'];
      $nationalityid = $_POST['Editnationalityid'];
      $religionid = $_POST['Editreligionid'];
      $categoryid = $_POST['Editcategoryid'];
      $boardid = $_POST['Editboardid'];
      $hallticket = $_POST['Edithallticket'];
      $schoolname = $_POST['Editschoolname'];
      $grade = $_POST['Editgrade'];
      $placeofeducation = $_POST['Editplaceofeducation'];
      $door_street = $_POST['Editdoor_street'];
      $village_mandal = $_POST['Editvillage_mandal'];
      $landmark = $_POST['Editlandmark'];
      $city_town = $_POST['Editcity_town'];
      $district = $_POST['Editdistrict'];
      $pin = $_POST['Editpin'];
      $mobile1 = $_POST['Editmobile1'];
      $mobile2 = $_POST['Editmobile2'];
      $email = $_POST['Editemail'];
      $admissiontypeid = $_POST['Editadmissiontypeid'];
      $courseid = $_POST['Editcourseid'];
      $termid = $_POST['Edittermid'];
      $secondlanguageid = $_POST['Editsecondlanguageid'];
      $branchid = $_POST['Editbranchid'];

      $sql = "UPDATE users SET name=:name, fathername=:fathername, mothername=:mothername, parentoccupation=:parentoccupation, studentaadhaar=:studentaadhaar, 
                               boardid=:boardid, hallticket=:hallticket, dateofbirth=:dateofbirth, nationalityid=:nationalityid, religionid=:religionid, 
                               categoryid=:categoryid, schoolname=:schoolname, grade=:grade, placeofeducation=:placeofeducation, genderid=:genderid, 
                               admissiontypeid=:admissiontypeid, branchid=:branchid, courseid=:courseid, termid=:termid, secondlanguageid=:secondlanguageid, 
                               door_street=:door_street, village_mandal=:village_mandal, landmark=:landmark, city_town=:city_town, district=:district, 
                               pin=:pin, mobile1=:mobile1, mobile2=:mobile2, email=:email WHERE userid=:userid";

      $query = $dbh->prepare($sql);
      $query->bindParam(':userid', $userid, PDO::PARAM_STR);

      $query->bindParam(':name', $name, PDO::PARAM_STR);
      $query->bindParam(':fathername', $fathername, PDO::PARAM_STR);
      $query->bindParam(':mothername', $mothername, PDO::PARAM_STR);
      $query->bindParam(':parentoccupation', $parentoccupation, PDO::PARAM_STR);
      $query->bindParam(':studentaadhaar', $studentaadhaar, PDO::PARAM_STR);
      $query->bindParam(':boardid', $boardid, PDO::PARAM_STR);
      $query->bindParam(':hallticket', $hallticket, PDO::PARAM_STR);
      $query->bindParam(':dateofbirth', $dateofbirth, PDO::PARAM_STR);
      $query->bindParam(':nationalityid', $nationalityid, PDO::PARAM_STR);
      $query->bindParam(':religionid', $religionid, PDO::PARAM_STR);
      $query->bindParam(':categoryid', $categoryid, PDO::PARAM_STR);
      $query->bindParam(':schoolname', $schoolname, PDO::PARAM_STR);
      $query->bindParam(':grade', $grade, PDO::PARAM_STR);
      $query->bindParam(':placeofeducation', $placeofeducation, PDO::PARAM_STR);
      $query->bindParam(':genderid', $genderid, PDO::PARAM_STR);
      $query->bindParam(':admissiontypeid', $admissiontypeid, PDO::PARAM_STR);
      $query->bindParam(':branchid', $branchid, PDO::PARAM_STR);
      $query->bindParam(':courseid', $courseid, PDO::PARAM_STR);
      $query->bindParam(':termid', $termid, PDO::PARAM_STR);
      $query->bindParam(':secondlanguageid', $secondlanguageid, PDO::PARAM_STR);
      $query->bindParam(':door_street', $door_street, PDO::PARAM_STR);
      $query->bindParam(':village_mandal', $village_mandal, PDO::PARAM_STR);
      $query->bindParam(':landmark', $landmark, PDO::PARAM_STR);
      $query->bindParam(':city_town', $city_town, PDO::PARAM_STR);
      $query->bindParam(':district', $district, PDO::PARAM_STR);
      $query->bindParam(':pin', $pin, PDO::PARAM_STR);
      $query->bindParam(':mobile1', $mobile1, PDO::PARAM_STR);
      $query->bindParam(':mobile2', $mobile2, PDO::PARAM_STR);
      $query->bindParam(':email', $email, PDO::PARAM_STR);

      $query->execute();
      header("location:student_details.php?student_id={$userid}");
   }

   if (isset($_POST['add_photo'])) {

      $student_photo = $_FILES['student_photo']['name'];
      $student_photo_doc = $_FILES['student_photo']['tmp_name'];

      $userId = $_GET['student_id'];
      $imagename = $userId . ".png";
      move_uploaded_file($student_photo_doc, "images/$imagename");

      $sql = "UPDATE users SET image='1' WHERE userid=$userId";
      $query = $dbh->prepare($sql);
      $query->execute();
   }
?>
   <!doctype html>
   <html lang="en" class="no-js">

   <head>
      <?php include('includes/header.php'); ?>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js'></script>
   </head>

   <body>
      <div class="ts-main-content">
         <?php include('includes/leftbar.php'); ?>
         <div class="content-wrapper">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
                     <h2 class="page-title">Student Details</h2>
                     <div class="row">
                        <div class="col-md-12">
                           <ul class="nav nav-pills red">
                              <li class="nav-item active"><a class="nav-link active" data-toggle="tab" href="#summary">Summary</a></li>
                              <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">Profile</a></li>
                              <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoices">Invoices</a></li>
                              <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Payments">Payments</a></li>
                           </ul>

                           <?php

                           $s_id = $_GET['student_id'];
                           $sql = "SELECT * from users 
                                                JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                JOIN courselookup ON users.courseid = courselookup.courseid
                                                JOIN applicationstatuslookup ON users.applicationstatusid = applicationstatuslookup.applicationstatusid
                                                JOIN genderlookup ON users.genderid = genderlookup.genderid
                                                JOIN termlookup ON users.termid = termlookup.termid
                                                JOIN nationalitylookup ON users.nationalityid = nationalitylookup.nationalityid
                                                JOIN religionlookup ON users.religionid = religionlookup.religionid
                                                JOIN categorylookup ON users.categoryid = categorylookup.categoryid
                                                JOIN boardlookup ON users.boardid = boardlookup.boardid
                                                WHERE roleid = 5 and userid = $s_id";

                           $query = $dbh->prepare($sql);
                           $query->execute();
                           $results = $query->fetchAll(PDO::FETCH_OBJ);
                           foreach ($results as $result) : ?>
                              <div class="tab-content">
                                 <div id="summary" class="tab-pane fade in active">
                                    <br />
                                    <div class="col-md-2">
                                       <img id="output_image" src="<?php echo $result->image != "1" ? "images/user-image.png" : "images/{$result->userid}.png" ?>" width="140"><br />
                                       <form method="post" enctype="multipart/form-data">
                                          <input accept="image/*" onchange="preview_image(event)" type="file" style="display: none;" id="student_photo" name="student_photo" class="form-control mb">
                                          <br />
                                          <button type="submit" name="add_photo" class="btn btn-primary">Upload Photo</button>
                                       </form>

                                    </div>
                                    <div class="col-md-5">
                                       <table class="table table-bordered">
                                          <thead>
                                             <tr>
                                                <th style="background-color: #00203d; color: white;" colspan="3">Personal Details</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr>
                                                <td><b>Name</td>
                                                <td colspan="2"><?php echo $result->name ?></td>
                                             </tr>
                                             <tr>
                                                <td><b>D.O.B</td>
                                                <td colspan="2"><?php echo $result->dateofbirth ?></td>
                                             </tr>
                                             <tr>
                                                <td><b>Mobile</td>
                                                <td colspan="2"><?php echo $result->mobile1 ?></td>
                                             </tr>
                                             <tr>
                                                <td><b>E-Mail</td>
                                                <td colspan="2"><?php echo $result->email ?></td>
                                             </tr>
                                             <tr>
                                                <td><b>Address</td>
                                                <td colspan="2"><?php echo $result->door_street ?>, <?php echo $result->village_mandal ?>, <?php echo $result->landmark ?>, <?php echo $result->city_town ?>, <?php echo $result->district ?> - <?php echo $result->pin ?></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>

                                    <div class="col-md-5">
                                       <table class="table table-bordered">
                                          <thead>
                                             <tr>
                                                <th style="background-color: #00203d; color: white;" colspan="3">Course Details</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr>
                                                <td><b>Academic Year/Term</td>
                                                <td colspan="2"><?php echo $result->termname ?></td>
                                             </tr>
                                             <tr>
                                                <td><b>Course</td>
                                                <td colspan="2"><?php echo $result->coursename ?></td>
                                             </tr>
                                             <tr>
                                                <td><b>Admission Type</td>
                                                <td colspan="2"><?php echo $result->admissiontypename ?></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>

                                 <div id="profile" class="tab-pane fade">
                                    <br />
                                    <form method="post" id="editstudentform">

                                       <ul class="nav nav-tabs">
                                          <li class="active"><a id="EditBasicTab" data-toggle="tab" href="#Editbasic">Basic Details</a></li>
                                          <li><a id="EditFamilyTab" data-toggle="tab" href="#Editfamily">Family Details</a></li>
                                          <li><a id="EditEducationTab" data-toggle="tab" href="#Editeducation">Education Details</a></li>
                                          <li><a id="EditContactTab" data-toggle="tab" href="#Editcontact">Contact Details</a></li>
                                          <li><a id="EditApplicationTab" data-toggle="tab" href="#Editapplication">Application Details</a></li>
                                       </ul>
                                       <div class="tab-content tab-validate">
                                          <div id="Editbasic" class="tab-pane fade in active">
                                             <br>
                                             <div class="row">
                                                <div class="col-md-6">

                                                   <label for="" class="text-uppercase text-sm">Student Name</label>
                                                   <input type="text" placeholder="Name" id="Editname" name="Editname" class="form-control mb" value="<?php echo $result->name ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Date Of Birth</label>
                                                   <input type="text" placeholder="yyyy-mm-dd" id="Editdateofbirth" name="Editdateofbirth" class="form-control mb datepicker" value="<?php echo $result->dateofbirth ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editgenderid" name="Editgenderid" style="width: 100%;" class="select2 form-control mb select" required>
                                                      <option value="">Select Gender</option>
                                                      <?php
                                                      foreach ($genderlookup as $gender) :
                                                      ?>
                                                         <option <?php echo $result->genderid == $gender->genderid ? "selected" : ""; ?> value="<?php echo $gender->genderid; ?>"><?php echo $gender->gendername; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                   <br><br>
                                                </div>
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editnationalityid" name="Editnationalityid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Nationality</option>
                                                      <?php
                                                      foreach ($nationalitylookup as $nationality) :
                                                      ?>
                                                         <option <?php echo $result->nationalityid == $nationality->nationalityid ? "selected" : "" ?> value="<?php echo $nationality->nationalityid; ?>"><?php echo $nationality->nationalityname; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editreligionid" name="Editreligionid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Religion</option>
                                                      <?php
                                                      foreach ($religionlookup as $religion) :
                                                      ?>
                                                         <option <?php echo $result->religionid == $religion->religionid ? "selected" : "" ?> value="<?php echo $religion->religionid; ?>"><?php echo $religion->religionname; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                   <br><br>
                                                </div>
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editcategoryid" name="Editcategoryid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Category</option>
                                                      <?php
                                                      foreach ($categorylookup as $category) :
                                                      ?>
                                                         <option <?php echo $result->categoryid == $category->categoryid ? "selected" : "" ?> value="<?php echo $category->categoryid; ?>"><?php echo $category->categoryname; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Aadhaar Number</label>
                                                   <input type="text" placeholder="Aadhaar Number" id="Editstudentaadhaar" name="Editstudentaadhaar" class="form-control mb" value="<?php echo $result->studentaadhaar ?>" required>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="Editfamily" class="tab-pane fade">
                                             <br>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Father Name/ Guardian Name</label>
                                                   <input type="text" placeholder="Father Name" id="Editfathername" name="Editfathername" class="form-control mb" value="<?php echo $result->fathername ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Mother Name</label>
                                                   <input type="text" placeholder="Mother Name" id="Editmothername" name="Editmothername" class="form-control mb" value="<?php echo $result->mothername ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Parent Occupation</label>
                                                   <input type="text" placeholder="Parent Occupation" id="Editparentoccupation" name="Editparentoccupation" class="form-control mb" value="<?php echo $result->parentoccupation ?>" required>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="Editeducation" class="tab-pane fade">
                                             <br>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <label for="" class="text-uppercase text-sm">Which board did the student study in?</label>
                                                   <select onchange="this.classList.remove('error')" id="Editboardid" name="Editboardid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Board</option>
                                                      <?php
                                                      foreach ($boardlookup as $board) :
                                                      ?>
                                                         <option <?php echo $result->boardid == $board->boardid ? "selected" : "" ?> value="<?php echo $board->boardid; ?>"><?php echo $board->boardname; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                   <br><br>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">School Name</label>
                                                   <input type="text" placeholder="School Name" id="Editschoolname" name="Editschoolname" class="form-control mb" value="<?php echo $result->schoolname ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Grade/ Marks</label>
                                                   <input type="text" placeholder="Grade/Marks" id="Editgrade" name="Editgrade" class="form-control mb" value="<?php echo $result->grade ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Hallticket Number</label>
                                                   <input type="text" placeholder="Hallticket Number" id="Edithallticket" name="Edithallticket" class="form-control mb" value="<?php echo $result->hallticket ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Area/ Place</label>
                                                   <input type="text" placeholder="Place of education" id="Editplaceofeducation" name="Editplaceofeducation" class="form-control mb" value="<?php echo $result->placeofeducation ?>" required>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="Editcontact" class="tab-pane fade">
                                             <br>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Door No./ Street Name</label>
                                                   <input type="text" placeholder="Door No./ Street Name" id="Editdoor_street" name="Editdoor_street" class="form-control mb" value="<?php echo $result->door_street ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Village/ Mandal</label>
                                                   <input type="text" placeholder="Village/ Mandal" id="Editvillage_mandal" name="Editvillage_mandal" class="form-control mb" value="<?php echo $result->village_mandal ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Landmark</label>
                                                   <input type="text" placeholder="Landmark" id="Editlandmark" name="Editlandmark" class="form-control mb" value="<?php echo $result->landmark ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">City/ Town</label>
                                                   <input type="text" placeholder="City/ Town" id="Editcity_town" name="Editcity_town" class="form-control mb" value="<?php echo $result->city_town ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">District</label>
                                                   <input type="text" placeholder="District" id="Editdistrict" name="Editdistrict" class="form-control mb" value="<?php echo $result->district ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Pin</label>
                                                   <input type="text" placeholder="Pin" id="Editpin" name="Editpin" class="form-control mb" value="<?php echo $result->pin ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Mobile 1</label>
                                                   <input type="number" placeholder="Mobile 1" id="Editmobile1" name="Editmobile1" class="form-control mb" value="<?php echo $result->mobile1 ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="" class="text-uppercase text-sm">Mobile 2</label>
                                                   <input type="number" placeholder="Mobile 2" id="Editmobile2" name="Editmobile2" class="form-control mb" value="<?php echo $result->mobile2 ?>" required>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <label for="" class="text-uppercase text-sm">Email Address</label>
                                                   <input type="text" placeholder="Email Address" id="Editemail" name="Editemail" class="form-control mb" value="<?php echo $result->email ?>" required>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="Editapplication" class="tab-pane fade">
                                             <br>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <label for="" class="text-uppercase text-sm">Application Number</label>
                                                   <input type="text" placeholder="Application Number" id="Editapplicationnumber" name="Editapplicationnumber" class="form-control" value="<?php echo $result->applicationnumber ?>" disabled required>
                                                </div>
                                             </div>
                                             <br />
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error');" id="Editadmissiontypeid" name="Editadmissiontypeid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Admission Type</option>
                                                      <?php
                                                      foreach ($admissiontypelookup as $admissiontype) :
                                                      ?>
                                                         <option <?php echo $result->admissiontypeid == $admissiontype->admissiontypeid ? "selected" : "" ?> value="<?php echo $admissiontype->admissiontypeid; ?>"><?php echo $admissiontype->admissiontypename; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                   <br><br>
                                                </div>
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editbranchid" name="Editbranchid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Branch</option>
                                                      <?php
                                                      foreach ($branchlookup as $branch) :
                                                      ?>
                                                         <option <?php echo $result->branchid == $branch->branchid ? "selected" : "" ?> value="<?php echo $branch->branchid; ?>"><?php echo $branch->city . ", " . $branch->campus; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                   <br><br>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editcourseid" name="Editcourseid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Course</option>
                                                      <?php
                                                      foreach ($courselookup as $course) :
                                                      ?>
                                                         <option <?php echo $result->courseid == $course->courseid ? "selected" : "" ?> value="<?php echo $course->courseid; ?>"><?php echo $course->coursename; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                   <br><br>
                                                </div>
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Edittermid" name="Edittermid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Term</option>
                                                      <?php
                                                      foreach ($termlookup as $term) :
                                                      ?>
                                                         <option <?php echo $result->termid == $term->termid ? "selected" : "" ?> value="<?php echo $term->termid; ?>"><?php echo $term->termname; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <select onchange="this.classList.remove('error')" id="Editsecondlanguageid" name="Editsecondlanguageid" style="width: 100%;" class="select2 form-control mb" required>
                                                      <option value="">Select Second Language</option>
                                                      <?php
                                                      foreach ($secondlanguagelookup as $secondlanguage) :
                                                      ?>
                                                         <option <?php echo $result->secondlanguageid == $secondlanguage->secondlanguageid ? "selected" : "" ?> value="<?php echo $secondlanguage->secondlanguageid; ?>"><?php echo $secondlanguage->secondlanguagename; ?></option>
                                                      <?php
                                                      endforeach;
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <br />
                                       <input type="hidden" id="Edituserid" name="Edituserid" value="<?php echo $result->userid; ?>" />
                                       <button type="submit" id="updatestudent" name="updatestudent" class="btn btn-primary">Update Student</button>
                                    </form>
                                 </div>
                                 <div id="invoices" class="tab-pane fade">
                                    <br>
                                    <table id="tblinvoices" class="DataTable table table-striped">
                                       <thead>
                                          <tr>
                                             <th>InvoiceId</th>
                                             <th>Name</th>
                                             <th>Branch</th>
                                             <th>Course - Term - Admission Type</th>
                                             <th>Total Invoice Value</th>
                                             <th>Total Paid</th>
                                             <th>Remaining Amount</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                          if ($_SESSION['userdetails']->branchid == NULL && $_GET['student_id']) {
                                             $s_id = $_GET['student_id'];
                                             $sql =
                                                "SELECT InvoiceDetails.userid,InvoiceDetails.TotalValue, InvoiceDetails.invoiceid, InvoiceDetails.name, InvoiceDetails.city, InvoiceDetails.campus, InvoiceDetails.admissiontypename, InvoiceDetails.coursename, InvoiceDetails.termname,
                                    PaymentDetails.TotalPaid, (InvoiceDetails.TotalValue - PaymentDetails.TotalPaid) AS RemainingAmount FROM
                                    (SELECT SUM(invoices.feesvalue) as TotalValue, invoices.invoiceid, users.userid, users.name, branchlookup.city, branchlookup.campus, admissiontypelookup.admissiontypename, courselookup.coursename, termlookup.termname  
                                                                                                 from invoices JOIN users ON invoices.userid = users.userid 
                                                                                                               JOIN feestructurelookup ON invoices.feesid = feestructurelookup.feesid
                                                                                                               JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                                                                               JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                                                                               JOIN courselookup ON users.courseid = courselookup.courseid
                                                                                                               JOIN termlookup ON users.termid = termlookup.termid
                                                                                                               GROUP BY invoices.invoiceid) InvoiceDetails
                                                                                                               LEFT JOIN
                                                                                                               (SELECT SUM(payments.paymentamount) as TotalPaid, payments.userid FROM payments JOIN users ON payments.userid = users.userid GROUP BY payments.userid) PaymentDetails ON InvoiceDetails.userid = PaymentDetails.userid WHERE InvoiceDetails.userid=$s_id";
                                          } else {
                                             $sql =
                                                "SELECT InvoiceDetails.userid,InvoiceDetails.TotalValue, InvoiceDetails.invoiceid, InvoiceDetails.name, InvoiceDetails.city, InvoiceDetails.campus, InvoiceDetails.admissiontypename, InvoiceDetails.coursename, InvoiceDetails.termname,
                                    PaymentDetails.TotalPaid, (InvoiceDetails.TotalValue - PaymentDetails.TotalPaid) AS RemainingAmount FROM
                                    (SELECT SUM(invoices.feesvalue) as TotalValue, invoices.invoiceid, users.userid, users.name, branchlookup.city, branchlookup.campus, admissiontypelookup.admissiontypename, courselookup.coursename, termlookup.termname  
                                                                                                 from invoices JOIN users ON invoices.userid = users.userid 
                                                                                                               JOIN feestructurelookup ON invoices.feesid = feestructurelookup.feesid
                                                                                                               JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                                                                               JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                                                                               JOIN courselookup ON users.courseid = courselookup.courseid
                                                                                                               JOIN termlookup ON users.termid = termlookup.termid
                                                                                                               WHERE users.branchid IN ({$_SESSION['userdetails']->branchid})
                                                                                                               GROUP BY invoices.invoiceid) InvoiceDetails
                                                                                                               LEFT JOIN
                                                                                                               (SELECT SUM(payments.paymentamount) as TotalPaid, payments.userid FROM payments JOIN users ON payments.userid = users.userid GROUP BY payments.userid) PaymentDetails ON InvoiceDetails.userid = PaymentDetails.userid WHERE InvoiceDetails.userid=$s_id";
                                          }


                                          $query = $dbh->prepare($sql);
                                          $query->execute();
                                          $results = $query->fetchAll(PDO::FETCH_OBJ);

                                          foreach ($results as $result) :
                                          ?>
                                             <tr>
                                                <td><a target="_blank" href="includes/generateinvoice.php?invoiceid=<?php echo $result->invoiceid ?>"><?php echo $result->invoiceid ?></a></td>
                                                <td><?php echo $result->name ?></td>
                                                <td><?php echo $result->city . ", " . $result->campus ?></td>
                                                <td><?php echo $result->coursename . ", " . $result->termname . ', ' . $result->admissiontypename ?></td>
                                                <td><?php echo $result->TotalValue ?></td>
                                                <td><?php echo $result->TotalPaid == NULL ? "0.00" : $result->TotalPaid ?></td>
                                                <td><?php echo $result->RemainingAmount == NULL ? $result->TotalValue : $result->RemainingAmount ?></td>
                                             </tr>
                                          <?php endforeach; ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div id="Payments" class="tab-pane fade">
                                    <br>
                                    <a class="btn btn-success" style="background-color: #00203d;" data-toggle="modal" data-target="#addpayment">Add Payment</a>
                                    <br><br>
                                    <table id="tblPayments" class="DataTable table table-striped">
                                       <thead>
                                          <tr>
                                             <th>Application Number</th>
                                             <th>Name</th>
                                             <th>Payment Status</th>
                                             <th>Payment</th>
                                             <th>Payment Date</th>
                                             <th>Payment Type</th>
                                             <th>Payment Details</th>
                                             <th>Amount Received By</th>
                                             <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_edit == 1) : ?>
                                                <th>Edit</th>
                                             <?php endif; ?>
                                             <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_delete == 1) : ?>
                                                <th>Delete</th>
                                             <?php endif; ?>
                                          </tr>
                                       </thead>

                                       <tbody>
                                          <?php
                                          $id = $_GET['student_id'];
                                          if ($_SESSION['userdetails']->branchid == NULL) {


                                             $sql =
                                                $sql =
                                                "SELECT U.applicationnumber, U.name, PSL.paymentstatusname, paymentid, paymentamount, paymentdate, paymenttypename, otherdetails, PC.name as receivedby 
                                                     FROM payments P 
                                                     JOIN users U ON P.userid = U.userid 
                                                     JOIN paymentstatuslookup PSL ON P.paymentstatusid = PSL.paymentstatusid 
                                                     JOIN paymenttypelookup PT ON P.paymenttypeid = PT.paymenttypeid 
                                                     LEFT JOIN users PC ON P.paymentcollectedby = PC.userid WHERE U.userid=$id";
                                          } else {
                                             $sql =
                                                "SELECT U.applicationnumber, U.name, PSL.paymentstatusname, paymentid, paymentamount, paymentdate, paymenttypename, otherdetails, PC.name as receivedby 
                                                     FROM payments P 
                                                     JOIN users U ON P.userid = U.userid 
                                                     JOIN paymentstatuslookup PSL ON P.paymentstatusid = PSL.paymentstatusid 
                                                     JOIN paymenttypelookup PT ON P.paymenttypeid = PT.paymenttypeid 
                                                     LEFT JOIN users PC ON P.paymentcollectedby = PC.userid 
                                                     WHERE U.userid=$id and U.branchid IN ({$_SESSION['userdetails']->branchid})";
                                          }

                                          $query = $dbh->prepare($sql);
                                          $query->execute();
                                          $results = $query->fetchAll(PDO::FETCH_OBJ);

                                          foreach ($results as $result) :


                                          ?>

                                             <tr>
                                                <td><?php echo $result->applicationnumber ?></td>
                                                <td><?php echo $result->name ?></td>
                                                <td><?php echo $result->paymentstatusname ?></td>
                                                <td><?php echo $result->paymentamount ?></td>
                                                <td><?php echo $result->paymentdate ?></td>
                                                <td><?php echo $result->paymenttypename ?></td>
                                                <td><?php echo $result->otherdetails ?></td>
                                                <td><?php echo $result->receivedby ?></td>
                                                <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_edit == 1) : ?>
                                                   <td><i class="fa fa-pencil-square-o"></i></td>
                                                <?php endif; ?>

                                                <?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_delete == 1) : ?>
                                                   <td><i class="fa fa-trash"></i></td>
                                                <?php endif; ?>
                                             </tr>
                                          <?php endforeach; ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="modal fade" id="addpayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h2 class="modal-title" id="exampleModalLabel">Add Payment</h2>
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span>&times;</span>
                                             </button>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post">
                                                <br>
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <select name="userid" style="width: 100%;" class="select2 form-control mb" required>
                                                         <option value="">Select Student</option>
                                                         <?php

                                                         if ($_SESSION['userdetails']->branchid == NULL && $_GET['student_id']) {
                                                            $s_id = $_GET['student_id'];
                                                            $sql = "SELECT * from users WHERE userid = $s_id";
                                                         } else {
                                                            $sql = "SELECT * from users WHERE userid = $s_id and users.branchid IN ({$_SESSION['userdetails']->branchid})";
                                                         }
                                                         $query = $dbh->prepare($sql);
                                                         $query->execute();
                                                         $studentdetails = $query->fetchAll(PDO::FETCH_OBJ);

                                                         foreach ($studentdetails as $student) :
                                                         ?>
                                                            <option value="<?php echo $student->userid; ?>"><?php echo $student->applicationnumber . " - " .  $student->name; ?></option>
                                                         <?php
                                                         endforeach;
                                                         ?>
                                                      </select>
                                                      <br><br>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <select name="paymenttypeid" style="width: 100%;" class="select2 form-control mb" required>
                                                         <option value="">Select Payment Type</option>
                                                         <?php

                                                         foreach ($paymenttypelookup as $paymenttype) :
                                                         ?>
                                                            <option value="<?php echo $paymenttype->paymenttypeid; ?>"><?php echo $paymenttype->paymenttypename; ?></option>
                                                         <?php
                                                         endforeach;
                                                         ?>
                                                      </select>
                                                   </div>
                                                </div>

                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <label for="" class="text-uppercase text-sm">Payment Amount</label>
                                                      <input type="number" placeholder="Payment Amount" name="paymentamount" class="form-control mb" required>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <label for="" class="text-uppercase text-sm">Payment Details</label>
                                                      <input type="text" placeholder="UTR No / Check No / Recipt No" name="otherdetails" class="form-control mb" required>
                                                   </div>
                                                </div>
                                                <br />
                                                <button type="submit" name="addpayment" class="btn btn-primary">Add Payment</button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           <?php endforeach; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>

   </html>
   <style>
      input.error {
         border-color: #f00 !important;
      }

      select.error+span>span>span {
         border-color: #f00 !important;
      }

      small.required {
         color: #f00;
      }

      .error {
         color: red;
      }
   </style>
   <!-- Loading Scripts -->
   <script>
      $('#output_image').click(function() {
         $('#student_photo').click();
      });

      window.onload = function() {
         $('.DataTable').DataTable();
         $('.select2').select2();
      }

      $('#editstudentform').validate({
         ignore: [],
         submitHandler: function() {
            form.submit();
         },
         errorPlacement: function(error, element) {

         },
         invalidHandler: function() {
            setTimeout(function() {
               $('.nav-tabs a small.required').remove();
               var validatePane = $('.tab-content.tab-validate .tab-pane:has(input.error)').each(function() {
                  var id = $(this).attr('id');
                  $('.nav-tabs').find('a[href^="#' + id + '"]').append(' <small class="required">*</small>');
               });
               var validatePaneSelect = $('.tab-content.tab-validate .tab-pane:has(select.error)').each(function() {
                  var id = $(this).attr('id');
                  $('.nav-tabs').find('a[href^="#' + id + '"]').append(' <small class="required">*</small>');
               });
            });
         }
      });

      function preview_image(event) {
         var reader = new FileReader();
         reader.onload = function() {
            var output = document.getElementById('output_image');
            output.src = reader.result;
         }
         reader.readAsDataURL(event.target.files[0]);
      }
   </script>
<?php } ?>