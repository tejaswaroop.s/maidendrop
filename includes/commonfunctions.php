<?php 

function get_batch()
{
   $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

   $sql = "SELECT * FROM batch where isactive = 1";
   $query = $dbh->prepare($sql);
   $query->execute();
   $batch = $query->fetchAll(PDO::FETCH_OBJ);
   if (count($batch) > 0) {
      return $batch[0]->year;
   }
}

function get_paymentidcounter()
{
   $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

   $sql = "SELECT * FROM paymentidcounter";
   $query = $dbh->prepare($sql);
   $query->execute();
   $paymentidcounter = $query->fetchAll(PDO::FETCH_OBJ);
   if (count($paymentidcounter) > 0) {
      return $paymentidcounter[0]->nextpaymentid;
   }
}

function set_paymentidcounter()
{
   $current = get_paymentidcounter();

   $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

   $nextpaymentid = $current + 1;

   $sql = "UPDATE paymentidcounter SET nextpaymentid = {$nextpaymentid}";
   $query = $dbh->prepare($sql);
   $query->execute();
}

?>