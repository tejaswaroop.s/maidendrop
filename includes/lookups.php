<?php

if (COUNT($_SESSION) > 0) {
        $sql = "SELECT * from rights r 
                JOIN roleslookup rl ON rl.roleid = r.roleid
                JOIN operationslookup o ON r.operationid = o.operationid 
                WHERE r.roleid = {$_SESSION['userdetails']->roleid}";
        $query = $dbh->prepare($sql);
        $query->execute();
        $rights = $query->fetchAll(PDO::FETCH_OBJ);
        $_SESSION['rights'] = $rights;


        $sql = "SELECT * from paymenttypelookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $paymenttypelookup = $query->fetchAll(PDO::FETCH_OBJ);
        $paymenttypelookup_json = json_encode($paymenttypelookup);

        $sql = "SELECT * from paymentstatuslookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $paymentstatuslookup = $query->fetchAll(PDO::FETCH_OBJ);
        $paymentstatuslookup_json = json_encode($paymentstatuslookup);

        $sql = "SELECT * FROM approvalvaluelookup WHERE approvalvalueid = 1";
        $query = $dbh->prepare($sql);
        $query->execute();
        $approvalvaluelookup = $query->fetchAll(PDO::FETCH_OBJ);
        $approvalvaluelookup_json = json_encode($approvalvaluelookup);

        $sql = "SELECT * from roleslookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $roleslookup = $query->fetchAll(PDO::FETCH_OBJ);
        $roleslookup_json = json_encode($roleslookup);

        $sql = "SELECT * from operationslookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $operationslookup = $query->fetchAll(PDO::FETCH_OBJ);
        $operationslookup_json = json_encode($operationslookup);

        $sql = "SELECT * from feestructurelookup JOIN admissiontypelookup ON feestructurelookup.admissiontypeid = admissiontypelookup.admissiontypeid";
        $query = $dbh->prepare($sql);
        $query->execute();
        $feestructurelookup = $query->fetchAll(PDO::FETCH_OBJ);
        $feestructurelookup_json = json_encode($feestructurelookup);


        $sql = "SELECT * from admissiontypelookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $admissiontypelookup = $query->fetchAll(PDO::FETCH_OBJ);
        $admissiontypelookup_json = json_encode($admissiontypelookup);

        $sql = "SELECT * from applicationstatuslookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $applicationstatuslookup = $query->fetchAll(PDO::FETCH_OBJ);
        $applicationstatuslookup_json = json_encode($applicationstatuslookup);

        $sql = "SELECT * from boardlookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $boardlookup = $query->fetchAll(PDO::FETCH_OBJ);
        $boardlookup_json = json_encode($boardlookup);

        if ($_SESSION['userdetails']->branchid == NULL) {
                $sql = "SELECT * from branchlookup";
                $query = $dbh->prepare($sql);
                $query->execute();
                $branchlookup = $query->fetchAll(PDO::FETCH_OBJ);
                $branchlookup_json = json_encode($branchlookup);
        } else {
                $sql = "SELECT * from branchlookup where branchid in ({$_SESSION['userdetails']->branchid})";
                $query = $dbh->prepare($sql);
                $query->execute();
                $branchlookup = $query->fetchAll(PDO::FETCH_OBJ);
                $branchlookup_json = json_encode($branchlookup);
        }

        $sql = "SELECT * from categorylookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $categorylookup = $query->fetchAll(PDO::FETCH_OBJ);
        $categorylookup_json = json_encode($categorylookup);

        $sql = "SELECT * from courselookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $courselookup = $query->fetchAll(PDO::FETCH_OBJ);
        $courselookup_json = json_encode($courselookup);

        $sql = "SELECT * from termlookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $termlookup = $query->fetchAll(PDO::FETCH_OBJ);
        $termlookup_json = json_encode($termlookup);

        $sql = "SELECT * from genderlookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $genderlookup = $query->fetchAll(PDO::FETCH_OBJ);
        $genderlookup_json = json_encode($genderlookup);

        $sql = "SELECT * from nationalitylookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $nationalitylookup = $query->fetchAll(PDO::FETCH_OBJ);
        $nationalitylookup_json = json_encode($nationalitylookup);

        $sql = "SELECT * from religionlookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $religionlookup = $query->fetchAll(PDO::FETCH_OBJ);
        $religionlookup_json = json_encode($religionlookup);

        $sql = "SELECT * from secondlanguagelookup";
        $query = $dbh->prepare($sql);
        $query->execute();
        $secondlanguagelookup = $query->fetchAll(PDO::FETCH_OBJ);
        $secondlanguagelookup_json = json_encode($secondlanguagelookup);

        $sql = "SELECT * from users";
        $query = $dbh->prepare($sql);
        $query->execute();
        $userslookup = $query->fetchAll(PDO::FETCH_OBJ);
        $userslookup_json = json_encode($userslookup);
}
