<?php
session_start();
include('config.php');
include('lookups.php');

if (isset($_GET['paymentid'])) {
    $paymentid = $_GET['paymentid'];

    $sql =
        "SELECT payments.userid, payments.paymentid, payments.paymentamount, payments.paymentdate, paymenttypelookup.paymenttypename, payments.otherdetails, users.name as ReceivedBy 
         from payments LEFT JOIN users ON payments.paymentcollectedby = users.userid 
                            JOIN paymenttypelookup ON payments.paymenttypeid = paymenttypelookup.paymenttypeid WHERE payments.paymentid = '{$paymentid}'";

    $query = $dbh->prepare($sql);
    $query->execute();
    $paymentDetails = $query->fetchAll(PDO::FETCH_OBJ);

    if (count($paymentDetails) == 0) {
        echo "<html><body><h1>Payment Not Found</h1></body></html>";
    } else {

        $userid = $paymentDetails[0]->userid;

        $sql =
            "SELECT users.userid, users.name, branchlookup.city, branchlookup.campus, admissiontypelookup.admissiontypename, courselookup.coursename, termlookup.termname  
            FROM users
            JOIN branchlookup ON users.branchid = branchlookup.branchid
            JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
            JOIN courselookup ON users.courseid = courselookup.courseid
            JOIN termlookup ON users.termid = termlookup.termid WHERE users.userid = {$userid}";

        $query = $dbh->prepare($sql);
        $query->execute();
        $userDetails = $query->fetchAll(PDO::FETCH_OBJ);

?><html><body>
            <div class="ts-main-content">
                <div class="content-wrapper">
                    <div class="container-fluid">
                        <h1 style="text-align: center; background-color: #99cc33; color: white;"><strong>Payment Receipt</strong></h1>
                        <table style="width: 75%; margin-left: auto; margin-right: auto;">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">
                                        <p><img src="../images/logo.png" alt="" width="110" height="110" /></p>
                                    </td>
                                    <td style="width: 70%;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">
                                        <p><strong>MAIDENDROP GROUP</strong><br /><br />Plot No. 3-220, <br />Road No. 12,<br />Ayyappa Society, <br />Madhapur.<br />Phone: 9121144137/38</p>
                                    </td>
                                    <td style="width: 70%; text-align: right; vertical-align:bottom">
                                        <table style="width: 100%; margin-left: auto; margin-right: auto;">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color: #99cc33; border: 1px solid #99cc33;"><b>Name</b></td>
                                                    <td style="border: 1px solid #99cc33;"><i><?php echo $userDetails[0]->name ?></i></td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #99cc33; border: 1px solid #99cc33;"><b>City</b></td>
                                                    <td style="border: 1px solid #99cc33;"><i><?php echo $userDetails[0]->city ?></i></td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #99cc33; border: 1px solid #99cc33;"><b>Campus</b></td>
                                                    <td style="border: 1px solid #99cc33;"><i><?php echo $userDetails[0]->campus ?></i></td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #99cc33; border: 1px solid #99cc33;"><b>Admission Type</b></td>
                                                    <td style="border: 1px solid #99cc33;"><i><?php echo $userDetails[0]->admissiontypename ?></i></td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #99cc33; border: 1px solid #99cc33;"><b>Course</b></td>
                                                    <td style="border: 1px solid #99cc33;"><i><?php echo $userDetails[0]->coursename ?></i></td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #99cc33; border: 1px solid #99cc33;"><b>Term</b></td>
                                                    <td style="border: 1px solid #99cc33;"><i><?php echo $userDetails[0]->termname ?></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&nbsp;</p>
                        <?php
                        $totalPaid = 0;
                        ?>
                        <table style="width: 75%; margin-left: auto; margin-right: auto;">
                            <tbody>
                                <tr>
                                    <td style="background-color: #99cc33; width: 20%; text-align: center; border: 1px solid #99cc33;"><strong>Payment Id</strong></td>
                                    <td style="background-color: #99cc33; width: 20%; text-align: center; border: 1px solid #99cc33;"><strong>Payment Type</strong></td>
                                    <td style="background-color: #99cc33; width: 20%; text-align: center; border: 1px solid #99cc33;"><strong>Payment Date</strong></td>
                                    <td style="background-color: #99cc33; width: 20%; text-align: center; border: 1px solid #99cc33;"><strong>Other Details</strong></td>
                                    <td style="background-color: #99cc33; width: 20%; text-align: center; border: 1px solid #99cc33;"><strong>Payment Amount</strong></td>
                                </tr>
                                <?php
                                foreach ($paymentDetails as $payment) :
                                ?>
                                    <tr>
                                        <td style="width: 20%; text-align: center; border: 1px solid #99cc33;"><?php echo $payment->paymentid ?></td>
                                        <td style="width: 20%; text-align: center; border: 1px solid #99cc33;"><?php echo $payment->paymenttypename ?></td>
                                        <td style="width: 20%; text-align: center; border: 1px solid #99cc33;"><?php echo $payment->paymentdate ?></td>
                                        <td style="width: 20%; text-align: center; border: 1px solid #99cc33;"><?php echo $payment->otherdetails ?></td>
                                        <td style="width: 20%; text-align: center; border: 1px solid #99cc33;"><?php echo $payment->paymentamount ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <body><html>

        <?php }
} ?>