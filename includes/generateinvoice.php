<?php
session_start();
include('config.php');
include('lookups.php');

if (isset($_GET['invoiceid'])) {
    $invoiceid = $_GET['invoiceid'];

    $sql =
        "SELECT invoices.invoiceid, users.userid, users.name, branchlookup.city, branchlookup.campus, admissiontypelookup.admissiontypename, courselookup.coursename, termlookup.termname, feestructurelookup.feetype, invoices.feesvalue  
        from invoices 
        JOIN users ON invoices.userid = users.userid 
        JOIN feestructurelookup ON invoices.feesid = feestructurelookup.feesid
        JOIN branchlookup ON users.branchid = branchlookup.branchid
        JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
        JOIN courselookup ON users.courseid = courselookup.courseid
        JOIN termlookup ON users.termid = termlookup.termid WHERE invoices.invoiceid = '{$invoiceid}'";

    $query = $dbh->prepare($sql);
    $query->execute();
    $InvoiceDetails = $query->fetchAll(PDO::FETCH_OBJ);

    if (count($InvoiceDetails) == 0) {
        echo "<html><body><h1>Invoice Not Found</h1></body></html>";
    } else {

        $userid = $InvoiceDetails[0]->userid;

        $sql =
            "SELECT payments.paymentid, payments.paymentamount, payments.paymentdate, paymenttypelookup.paymenttypename, payments.otherdetails, users.name as ReceivedBy 
                    from payments LEFT JOIN users ON payments.paymentcollectedby = users.userid 
                                JOIN paymenttypelookup ON payments.paymenttypeid = paymenttypelookup.paymenttypeid WHERE payments.userid = {$userid}";

        $query = $dbh->prepare($sql);
        $query->execute();
        $PaymentDetails = $query->fetchAll(PDO::FETCH_OBJ);

?><html><body>
            <div class="ts-main-content">
                <div class="content-wrapper">
                    <div class="container-fluid">
                        <h1 style="text-align: center; background-color: #99cc33; color:white"><strong>Student Invoice</strong></h1>
                        <table style="width: 100%; margin-left: auto; margin-right: auto;">
                            <tbody>
                                <tr>
                                    <td style="width: 25%;">
                                        <p><img src="../images/logo.png" alt="" width="110" height="110" /></p>
                                        <p><strong>MAIDENDROP GROUP</strong><br />Plot No. 3-220, <br />Road No. 12,<br />Ayyappa Society, <br />Madhapur.<br />Phone: 9121144137/38</p>
                                        <p>
                                            Name: <?php echo $InvoiceDetails[0]->name ?><br />
                                            City: <?php echo $InvoiceDetails[0]->city ?><br />
                                            Campus: <?php echo $InvoiceDetails[0]->campus ?><br />
                                            Admission Type: <?php echo $InvoiceDetails[0]->admissiontypename ?><br />
                                            Course: <?php echo $InvoiceDetails[0]->coursename ?><br />
                                            Term: <?php echo $InvoiceDetails[0]->termname ?></p>
                                    </td>
                                    <td style="width: 75%;">
                                        <table style="width: 80%; margin-left: auto; margin-right: auto;">
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center; background-color: #99cc33; border:1px solid #99cc33"><strong>Invoice Number</strong></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center; border:1px solid #99cc33"><?php echo $invoiceid ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br />
                                        <table style="width: 80%; margin-left: auto; margin-right: auto;">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 75%; text-align: center; background-color: #99cc33; border:1px solid #99cc33"><strong>Fee Details</strong></td>
                                                    <td style="width: 25%; text-align: center; background-color: #99cc33; border:1px solid #99cc33"><strong>Amount</strong></td>
                                                </tr>
                                                <?php
                                                $total = 0;
                                                foreach ($InvoiceDetails as $invoice) :
                                                    $total = $total + $invoice->feesvalue;
                                                ?>
                                                    <tr>
                                                        <td style="width: 75%; text-align: center; border:1px solid #99cc33"><?php echo $invoice->feetype ?></td>
                                                        <td style="width: 25%; text-align: center; border:1px solid #99cc33"><?php echo $invoice->feesvalue ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                <tr>
                                                    <td style="width: 75%; text-align: center; background-color: #99cc33; border:1px solid #99cc33"><strong>Total</strong></td>
                                                    <td style="width: 25%; text-align: center; background-color: #99cc33; border:1px solid #99cc33; color:white"><b><?php echo $total ?></b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&nbsp;</p>
                        <?php
                        $totalPaid = 0;
                        ?>
                        <table style="width: 100%; margin-left: auto; margin-right: auto;">
                            <tbody>
                                <tr>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Payment Id</strong></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Payment Date</strong></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Payment Type</strong></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Other Details</strong></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Payment Received By</strong></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Amount Paid</strong></td>
                                </tr>
                                <?php
                                foreach ($PaymentDetails as $payment) :
                                    $totalPaid = $totalPaid + $payment->paymentamount;
                                ?>
                                    <tr>
                                        <td style="width: 16.66%; text-align: center; border:1px solid #99cc33"><?php echo $payment->paymentid ?></td>
                                        <td style="width: 16.66%; text-align: center; border:1px solid #99cc33"><?php echo $payment->paymentdate ?></td>
                                        <td style="width: 16.66%; text-align: center; border:1px solid #99cc33"><?php echo $payment->paymenttypename ?></td>
                                        <td style="width: 16.66%; text-align: center; border:1px solid #99cc33"><?php echo $payment->otherdetails ?></td>
                                        <td style="width: 16.66%; text-align: center; border:1px solid #99cc33"><?php echo $payment->ReceivedBy ?></td>
                                        <td style="width: 16.66%; text-align: center; border:1px solid #99cc33"><?php echo $payment->paymentamount ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"><strong>Balance Amount</strong></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33"></td>
                                    <td style="background-color: #99cc33; width: 16.66%; text-align: center; border:1px solid #99cc33; color:white"><b><?php echo $totalPaid ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </body></html>
<?php }
}
session_destroy();
?>