<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="images/logo.png" width="160" rel="icon">
	<link href="images/logo.png" width="32" rel="logo">

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/fileinput.min.css">
	<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script src="js/fileinput.js"></script>
	<script src="js/chartData.js"></script>
	<script src="js/main.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script>
		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	</script>
</head>
<div class="brand clearfix">
	<h4 class="pull-left text-white text-uppercase" style="margin:5px 0px 0px 20px"><img src="images/logo.png" height="50px"></img>
		&nbsp;&nbsp;&nbsp; <?php echo isset($_SESSION['userdetails']) ? $_SESSION['userdetails']->rolename . " Panel" : "";

							if (isset($_SESSION['userdetails'])) {
								if ($_SESSION['userdetails']->roleid != 1) {
									foreach ($branchlookup as $branch) {
										if ($branch->branchid == $_SESSION['userdetails']->branchid) {
											echo " - {$branch->city}" . ", {$branch->campus}";
										}
									}
								}
							}
							?>
	</h4>

	<?php if (count($_SESSION) > 0 && $_SESSION['userdetails'] != null) : ?>
		<div style="margin:20px 20px 20px 20px;float:right">
			<b style="color: white;">Search</b>
			<input type="text" id="tags" style="width: 25vw;" />
		</div>
	<?php endif; ?>

	<span class="menu-btn"><i class="fa fa-bars"></i></span>

	<ul class="ts-profile-nav">
		<li class="ts-profile-nav">
			<a href="">&nbsp;</a>
		</li>
	</ul>
</div>
<style>
	.ui-menu .ui-menu-item {
		background-color: #F7F4F4;
		margin: 3px;
	}

	.ui-autocomplete-row a {
		display: inline-block;
		text-decoration: none;
		width: 93%;
	}

	.ui-autocomplete-row a span {
		padding: 10px;
		font-size: 15px;
		text-align: center;
	}

	.ui-autocomplete-row a img {
		vertical-align: middle;
	}

	.ui-state-active,
	.ui-widget-content .ui-state-active {
		border: none;
		background: #EFEFEF;
		color: inherit;
	}

	.ui-menu .ui-state-focus,
	.ui-menu .ui-state-active {
		margin: 0;
	}
</style>
<script>
	$("#tags").autocomplete({
		source: "controller.php?Autocomplete=1",
		minLength: 0,
		position: { my : "right top", at: "right bottom" },
		select: function(event, ui) {
			window.location.href = "student_details.php?student_id=" + ui.item.id;
		}
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		return $("<li class='ui-autocomplete-row'></li>")
			.data("item.autocomplete", item)
			.append(item.label)
			.appendTo(ul);
	};
</script>