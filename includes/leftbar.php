	<nav class="ts-sidebar">
		<ul class="ts-sidebar-menu">
			<?php if ($_SESSION['rights'][array_search('Dashboard', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
				<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<?php endif; ?>
			<li data-toggle="collapse" data-target="#Users" class="collapsed active">
				<a href="#"><i class="fa fa-users"></i>Users</a><i class="fa fa-angle-double-right" hidden></i>
			</li>
			<ul class="sub-menu collapse" id="Users">
				<?php if ($_SESSION['userdetails']->userid == 1) : ?>
					<li style="margin-left: 30px;"><a href="employee.php"><i class="fa fa-users"></i>Employee</a></li>
				<?php endif; ?>
				<?php if ($_SESSION['rights'][array_search('Student', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
					<li style="margin-left: 30px;"><a href="student.php"><i class="fa fa-users"></i> Students</a></li>
				<?php endif; ?>
			</ul>


			<?php if ($_SESSION['rights'][array_search('Approval', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
				<li><a href="approval.php"><i class="fa fa-check"></i> Approvals</a></li>
			<?php endif; ?>
			<?php if ($_SESSION['rights'][array_search('Invoice', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
				<li><a href="invoice.php"><i class="fa fa-file-text"></i> Invoices</a></li>
			<?php endif; ?>
			<?php if ($_SESSION['rights'][array_search('Payment', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
				<li><a href="payment.php"><i class="fa fa-money"></i> Payments</a></li>
			<?php endif; ?>
			<?php if ($_SESSION['userdetails']->userid == 1) : ?>
				<li><a href="bulkupload.php"><i class="fa fa-users"></i> Bulk Upload</a></li>
			<?php endif; ?>
			<?php if ($_SESSION['userdetails']->userid == 1) : ?>
				<li><a href="rolesoperations.php"><i class="fa fa-arrow-circle-o-right"></i> Roles/ Operations</a></li>
			<?php endif; ?>
			<?php if ($_SESSION['userdetails']->userid == 1) : ?>
				<li><a href="rights.php"><i class="fa fa-arrow-circle-o-right"></i> Rights</a></li>
			<?php endif; ?>
			<li data-toggle="collapse" data-target="#Reports" class="collapsed active">
				<a href="#"><i class="fa fa-gift fa-lg"></i>Reports</a><i class="fa fa-angle-double-right" hidden></i>
			</li>
			<!-- <ul class="sub-menu collapse" id="Reports">
                    <li><a href="photo_upload.php">Photo Upload</a></li>
                </ul> -->
			<li><a href="changepassword.php"><i class="fa fa-key"></i> &nbsp;Change Password</a></li>
			<li><a href="logout.php"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
		</ul>
	</nav>