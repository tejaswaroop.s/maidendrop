<script>
    var admissiontypelookup_json = <?php echo $admissiontypelookup_json; ?>;
    var boardlookup_json = <?php echo $boardlookup_json; ?>;
    var branchlookup_json = <?php echo $branchlookup_json; ?>;
    var categorylookup_json = <?php echo $categorylookup_json; ?>;
    var courselookup_json = <?php echo $courselookup_json; ?>;
    var termlookup_json = <?php echo $termlookup_json; ?>;
    var genderlookup_json = <?php echo $genderlookup_json; ?>;
    var nationalitylookup_json = <?php echo $nationalitylookup_json; ?>;
    var religionlookup_json = <?php echo $religionlookup_json; ?>;
    var secondlanguagelookup_json = <?php echo $secondlanguagelookup_json; ?>;

    function generatePreview() {

        var fakeURL = "http://www.example.com/t.html?" + $('#studentform').serialize();
        var createURL = new URL(fakeURL);

        $('#preview_studentname').html(createURL.searchParams.get('name'));
        $('#preview_dateofbirth').html(createURL.searchParams.get('dateofbirth'));
        genderlookup_json.forEach((a) => {
            if (a.genderid == createURL.searchParams.get('genderid')) {
                $('#preview_gender').html(a.gendername)
            }
        });
        nationalitylookup_json.forEach((a) => {
            if (a.nationalityid == createURL.searchParams.get('nationalityid')) {
                $('#preview_nationality').html(a.nationalityname)
            }
        });
        religionlookup_json.forEach((a) => {
            if (a.religionid == createURL.searchParams.get('religionid')) {
                $('#preview_religion').html(a.religionname)
            }
        });
        categorylookup_json.forEach((a) => {
            if (a.categoryid == createURL.searchParams.get('categoryid')) {
                $('#preview_category').html(a.categoryname)
            }
        });
        $('#preview_aadhaar').html(createURL.searchParams.get('studentaadhaar'));

        $('#preview_fathername').html(createURL.searchParams.get('fathername'));
        $('#preview_mothername').html(createURL.searchParams.get('mothername'));
        $('#preview_parentoccupation').html(createURL.searchParams.get('parentoccupation'));

        boardlookup_json.forEach((a) => {
            if (a.boardid == createURL.searchParams.get('boardid')) {
                $('#preview_board').html(a.boardname)
            }
        });
        $('#preview_schoolname').html(createURL.searchParams.get('schoolname'));
        $('#preview_grade').html(createURL.searchParams.get('grade'));
        $('#preview_hallticketnumber').html(createURL.searchParams.get('hallticket'));
        $('#preview_area').html(createURL.searchParams.get('placeofeducation'));

        $('#preview_doorno').html(createURL.searchParams.get('door_street'));
        $('#preview_village').html(createURL.searchParams.get('village_mandal'));
        $('#preview_landmark').html(createURL.searchParams.get('landmark'));
        $('#preview_city').html(createURL.searchParams.get('city_town'));
        $('#preview_district').html(createURL.searchParams.get('district'));
        $('#preview_pin').html(createURL.searchParams.get('pin'));
        $('#preview_mobile1').html(createURL.searchParams.get('mobile1'));
        $('#preview_mobile2').html(createURL.searchParams.get('mobile2'));
        $('#preview_email').html(createURL.searchParams.get('email'));

        $('#preview_applicationnumber').html(createURL.searchParams.get('applicationnumber'));
        admissiontypelookup_json.forEach((a) => {
            if (a.admissiontypeid == createURL.searchParams.get('admissiontypeid')) {
                $('#preview_admissiontype').html(a.admissiontypename)
            }
        });
        branchlookup_json.forEach((a) => {
            if (a.branchid == createURL.searchParams.get('branchid')) {
                $('#preview_branch').html(a.city + ", " + a.campus)
            }
        });
        courselookup_json.forEach((a) => {
            if (a.courseid == createURL.searchParams.get('courseid')) {
                $('#preview_course').html(a.coursename)
            }
        });
        termlookup_json.forEach((a) => {
            if (a.termid == createURL.searchParams.get('termid')) {
                $('#preview_term').html(a.termname)
            }
        });
        secondlanguagelookup_json.forEach((a) => {
            if (a.secondlanguageid == createURL.searchParams.get('secondlanguageid')) {
                $('#preview_secondlanguage').html(a.secondlanguagename)
            }
        });

        $('#preview_coachingfee').html(createURL.searchParams.get('coachingfee'));
        $('#preview_comments').html(createURL.searchParams.get('comments'));

    }

    function loadEditControls(userid) {
        $.ajax({
            type: "GET",
            url: "controller.php",
            async: false,
            data: {
                getstudent: true,
                userid: userid
            },
            success: (msg) => {
                var userdata = JSON.parse(msg);

                $('#Edituserid').val(userid);

                $('#Editname').val(userdata.name);
                $('#Editdateofbirth').val(userdata.dateofbirth);
                $('#Editgenderid').val(userdata.genderid);
                $('#Editgenderid').select2();
                $('#Editnationalityid').val(userdata.nationalityid);
                $('#Editnationalityid').select2();
                $('#Editreligionid').val(userdata.religionid);
                $('#Editreligionid').select2();
                $('#Editcategoryid').val(userdata.categoryid);
                $('#Editcategoryid').select2();
                $('#Editstudentaadhaar').val(userdata.studentaadhaar);

                $('#Editfathername').val(userdata.fathername);
                $('#Editmothername').val(userdata.mothername);
                $('#Editparentoccupation').val(userdata.parentoccupation);

                $('#Editboardid').val(userdata.boardid);
                $('#Editboardid').select2();
                $('#Editreligionid').val(userdata.religionid);
                $('#Editreligionid').select2();
                $('#Editschoolname').val(userdata.schoolname);
                $('#Editgrade').val(userdata.grade);
                $('#Edithallticket').val(userdata.hallticket);
                $('#Editplaceofeducation').val(userdata.placeofeducation);

                $('#Editdoor_street').val(userdata.door_street);
                $('#Editvillage_mandal').val(userdata.village_mandal);
                $('#Editlandmark').val(userdata.landmark);
                $('#Editcity_town').val(userdata.city_town);
                $('#Editdistrict').val(userdata.district);
                $('#Editpin').val(userdata.pin);
                $('#Editmobile1').val(userdata.mobile1);
                $('#Editmobile2').val(userdata.mobile2);
                $('#Editemail').val(userdata.email);

                $('#Editapplicationnumber').val(userdata.applicationnumber);
                $('#Editadmissiontypeid').val(userdata.admissiontypeid);
                $('#Editadmissiontypeid').select2();
                $('#Editbranchid').val(userdata.branchid);
                $('#Editbranchid').select2();
                $('#Editcourseid').val(userdata.courseid);
                $('#Editcourseid').select2();
                $('#Edittermid').val(userdata.termid);
                $('#Edittermid').select2();
                $('#Editsecondlanguageid').val(userdata.secondlanguageid);
                $('#Editsecondlanguageid').select2();
            }
        });
    }

    function print_preview() {
        var contents = $("#print_preview").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({
            "position": "absolute",
            "top": "-1000000px"
        });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html>');
        frameDoc.document.write('<body>');
        frameDoc.document.write('<center><img src="images/logo.png" width="80" rel="icon"><br /><br /><hr style="width: 100%;" /><br /><br /></center>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function() {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
</script>