<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

$_SESSION = [];

if (isset($_SESSION['userdetails'])) {
	echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
}

if (isset($_POST['login'])) {
	$status = '1';
	$username = $_POST['username'];
	$password = md5($_POST['password']);
	$sql = "SELECT * FROM users 
			JOIN roleslookup on users.roleid = roleslookup.roleid 
			WHERE (username=:username OR employeeid=:employeeid) and password=:password";
	$query = $dbh->prepare($sql);
	$query->bindParam(':username', $username, PDO::PARAM_STR);
	$query->bindParam(':employeeid', $username, PDO::PARAM_STR);
	$query->bindParam(':password', $password, PDO::PARAM_STR);
	$query->execute();
	$results = $query->fetchAll(PDO::FETCH_OBJ);

	if ($query->rowCount() > 0) {
		$_SESSION['userdetails'] = $results[0];

		if ($results[0]->branchid == NULL) {
		} else {

			// $branchids = explode(',', $result->branchid);
			// $branches = "";
			// if ($result->branchid != null) {
			// 	foreach ($branchids as $branchid) {

			// 		$sql = "SELECT branchid, city, campus from branchlookup where branchid = '{$branchid}'";
			// 		$query = $dbh->prepare($sql);
			// 		$query->execute();
			// 		$results1 = $query->fetchAll(PDO::FETCH_OBJ);

			// 		$branches .= $results1[0]->city . "-" . $results1[0]->campus . ', ';
			// 	}
			// }

			$_SESSION['userdetails']->branchid = $results[0]->branchid;
		}

		$sql = "SELECT * from rights r 
						 JOIN roleslookup rl ON rl.roleid = r.roleid
						 JOIN operationslookup o ON r.operationid = o.operationid 
						 WHERE r.roleid = {$_SESSION['userdetails']->roleid}";
		$query = $dbh->prepare($sql);
		$query->execute();
		$results = $query->fetchAll(PDO::FETCH_OBJ);

		$_SESSION['rights'] = $results;
		header('location:dashboard.php');
	} else {
		echo "<script>alert('Invalid Details');</script>";
	}
}

?>
<!doctype html>
<html lang="en" class="no-js">

<head>
	<?php include('includes/header.php'); ?>
</head>

<body>
	<div class="login-page bk-img">
		<div class="form-content">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<h1 class="text-center text-bold mt-4x" style="color:white">Maiden Drop Login</h1>
						<div class="well row pt-2x pb-3x bk-light">
							<div class="col-md-8 col-md-offset-2">
								<form method="post">

									<label for="" class="text-uppercase text-sm">Username</label>
									<input type="text" placeholder="Username" name="username" class="form-control mb" required>

									<label for="" class="text-uppercase text-sm">Password</label>
									<input type="password" placeholder="Password" name="password" class="form-control mb" required>
									<button class="btn btn-primary btn-block" name="login" type="submit">LOGIN</button>
								</form>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>