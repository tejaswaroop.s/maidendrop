<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

$pageaccess = $_SESSION['rights'][array_search('Dashboard', array_column($_SESSION['rights'], 'operationname'))];
if ($pageaccess->_view != 1) {
	header('location:index.php');
} else {
	
?>
	<!doctype html>
	<html lang="en" class="no-js">

	<head>
		<?php include('includes/header.php'); ?>
	</head>

	<body>
		<div class="ts-main-content">
			<?php include('includes/leftbar.php'); ?>
			<div class="content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<h2 class="page-title"><?php echo $_SESSION['userdetails']->name . " " ?>Dashboard</h2>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<?php if ($_SESSION['rights'][array_search('TotalUsers', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<div class="panel panel-default">
													<div class="panel-body bk-primary text-light">
														<div class="stat-panel text-center">
															<?php
															$sql = "SELECT userid from users";
															$query = $dbh->prepare($sql);
															$query->execute();
															$results = $query->fetchAll(PDO::FETCH_OBJ);
															$bg = $query->rowCount();
															?>
															<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
															<div class="stat-panel-title text-uppercase">Users</div>
														</div>
													</div>
												</div>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('TotalSuperAdmins', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<div class="panel panel-default">
													<div class="panel-body bk-primary text-light">
														<div class="stat-panel text-center">
															<?php
															$sql = "SELECT userid from users where roleid = 1";
															$query = $dbh->prepare($sql);
															$query->execute();
															$results = $query->fetchAll(PDO::FETCH_OBJ);
															$bg = $query->rowCount();
															?>
															<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
															<div class="stat-panel-title text-uppercase">Super Admins</div>
														</div>
													</div>
												</div>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('TotalSeniorManagement', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="employee.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																$sql = "SELECT userid from users where roleid = 2";
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Senior Management</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('TotalCenterHeads', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="employee.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT userid from users WHERE roleid = 3";
																} else {
																	$sql = "SELECT userid from users WHERE roleid = 3 and users.branchid IN ({$_SESSION['userdetails']->branchid})";
																}
																
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Center Heads</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('TotalAdministrators', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="employee.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT userid from users WHERE roleid = 4";
																} else {
																	$sql = "SELECT userid from users WHERE roleid = 4 and users.branchid IN ({$_SESSION['userdetails']->branchid})";
																}
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Administrators</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('TotalStudents', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="student.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT userid from users WHERE roleid = 5 and applicationstatusid = 4";
																} else {
																	$sql = "SELECT userid from users WHERE roleid = 5 and applicationstatusid = 4 and users.branchid IN ({$_SESSION['userdetails']->branchid})";
																}
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Approved Students</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('TotalStudents', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="javascript::">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT userid from users WHERE roleid = 5 and applicationstatusid IN (1, 3)";
																} else {
																	$sql = "SELECT userid from users WHERE roleid = 5 and applicationstatusid IN (1, 3) and users.branchid IN ({$_SESSION['userdetails']->branchid})";
																}
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Pending Students</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('ViewPendingApprovals', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="approval.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	// $sql = "SELECT userid from users WHERE sendapprovalto = 'SA' and (applicationstatusid = 1 OR applicationstatusid = 3)";
																	$sql = "SELECT userid from users WHERE (applicationstatusid = 1 OR applicationstatusid = 3)";

																} else {
																	// $sql = "SELECT userid from users WHERE sendapprovalto = 'CH' and (applicationstatusid = 1 OR applicationstatusid = 3) and users.branchid = {$_SESSION['userdetails']->branchid}";
																	$sql = "SELECT userid from users WHERE (applicationstatusid = 1 OR applicationstatusid = 3) and users.branchid IN ({$_SESSION['userdetails']->branchid})";

																}
																
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Pending Student Approvals</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('ViewWaitingPaymentApprovals', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="payment.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT paymentid from payments WHERE paymentstatusid = 1";
																} else {
																	$sql = "SELECT paymentid from payments JOIN users ON users.userid = payments.userid WHERE paymentstatusid = 1 and users.branchid IN ({$_SESSION['userdetails']->branchid})";
																}
																
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																$bg = $query->rowCount();
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($bg); ?></div>
																<div class="stat-panel-title text-uppercase">Pending Payment Approvals</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('ViewTotalCollected', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="invoice.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT SUM(paymentamount) AS TotalCollected from payments WHERE paymentstatusid = 3";
																} else {
																	$sql = "SELECT SUM(paymentamount) AS TotalCollected from payments JOIN users ON payments.userid = users.userid WHERE users.branchid IN ({$_SESSION['userdetails']->branchid}) and payments.paymentstatusid = 3";
																}
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($results[0]->TotalCollected == null ? "0.00" : $results[0]->TotalCollected); ?></div>
																<div class="stat-panel-title text-uppercase">Total Collected</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('ViewTotalCollected', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-3">
												<a href="invoice.php">
													<div class="panel panel-default">
														<div class="panel-body bk-primary text-light">
															<div class="stat-panel text-center">
																<?php
																if ($_SESSION['userdetails']->branchid == NULL) {
																	$sql = "SELECT SUM(paymentamount) AS TotalCollected from payments WHERE paymentstatusid = 1";
																} else {
																	$sql = "SELECT SUM(paymentamount) AS TotalCollected from payments JOIN users ON payments.userid = users.userid WHERE users.branchid IN ({$_SESSION['userdetails']->branchid}) and payments.paymentstatusid = 1";
																}
																$query = $dbh->prepare($sql);
																$query->execute();
																$results = $query->fetchAll(PDO::FETCH_OBJ);
																?>
																<div class="stat-panel-number h1 "><?php echo htmlentities($results[0]->TotalCollected == null ? "0.00" : $results[0]->TotalCollected); ?></div>
																<div class="stat-panel-title text-uppercase">Pending Payment Amount</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php endif; ?>
										<?php if ($_SESSION['rights'][array_search('ViewTotalCollectedByBranch', array_column($_SESSION['rights'], 'operationname'))]->_view == 1) : ?>
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-body bk-primary text-light">
														<div class="stat-panel text-center">
															<?php
															$sql = "SELECT SUM(Derieved.amount) as TotalAmount, BL.city, BL.campus FROM 
															(SELECT SUM(paymentamount) as amount, U.branchid FROM payments P JOIN users U ON P.paymentcollectedby = U.userid GROUP BY U.branchid) Derieved 
															 JOIN branchlookup BL on Derieved.branchid = BL.branchid GROUP BY Derieved.branchid";
															$query = $dbh->prepare($sql);
															$query->execute();
															$results = $query->fetchAll(PDO::FETCH_OBJ);
															?>
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>
																			<center>Branch</center>
																		</th>
																		<th>
																			<center>Total Collected</center>
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<?php foreach ($results as $result) : ?>
																		<tr>
																			<td><?php echo $result->city . ", " . $result->campus ?></td>
																			<td><?php echo $result->TotalAmount ?></td>
																		</tr>
																	<?php endforeach; ?>
																</tbody>
															</table>
															<div class="stat-panel-title text-uppercase">Total Collected By Branch</div>
														</div>
													</div>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	</html>
<?php } ?>