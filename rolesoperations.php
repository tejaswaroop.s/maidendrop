<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

if ($_SESSION['userdetails']->userid == 1) :

    if (isset($_POST['createrole'])) {
        $rolename = $_POST['rolename'];

        $sql = "INSERT INTO roleslookup(rolename) 
                             VALUES(:rolename)";
        $query = $dbh->prepare($sql);
        $query->bindParam(':rolename', $rolename, PDO::PARAM_STR);
        $query->execute();

        $roleid = $dbh->lastInsertId();

        foreach ($operationslookup as $operation) {
            $sql = "INSERT INTO rights(roleid, operationid) 
                                     VALUES(:roleid,:operationid)";
            $query = $dbh->prepare($sql);
            $query->bindParam(':roleid', $roleid, PDO::PARAM_STR);
            $query->bindParam(':operationid', $operation->operationid, PDO::PARAM_STR);
            $query->execute();
        }
        header('location:rolesoperations.php');
    }

    if (isset($_POST['createoperation'])) {
        $operationname = $_POST['operationname'];
        $parent = $_POST['parent'] == "" ? 0 : $_POST['parent'];

        $sql = "INSERT INTO operationslookup(operationname, parent) 
                                     VALUES(:operationname,:parent)";
        $query = $dbh->prepare($sql);
        $query->bindParam(':operationname', $operationname, PDO::PARAM_STR);
        $query->bindParam(':parent', $parent, PDO::PARAM_STR);
        $query->execute();

        $operationid = $dbh->lastInsertId();

        foreach ($roleslookup as $role) {
            $sql = "INSERT INTO rights(roleid, operationid) 
                                     VALUES(:roleid,:operationid)";
            $query = $dbh->prepare($sql);
            $query->bindParam(':roleid', $role->roleid, PDO::PARAM_STR);
            $query->bindParam(':operationid', $operationid, PDO::PARAM_STR);
            $query->execute();
        }
        header('location:rolesoperations.php');
    }
?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Roles
                                <a class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#addroles">Add Role</a>
                            </h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tblseniormanagement" class="DataTable table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Role Name</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($roleslookup as $key => $role) :
                                            ?>
                                                <tr>
                                                    <td><?php echo $key + 1 ?></td>
                                                    <td><?php echo $role->rolename ?></td>
                                                    <td>
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-trash"></i>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="addroles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Add Role</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="rolename" class="text-uppercase text-sm">Role Name</label>
                                                    <input type="text" placeholder="Role Name" name="rolename" class="form-control mb" required>
                                                </div>
                                            </div>
                                            <button type="submit" name="createrole" class="btn btn-primary">Create Role</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Operations
                                <a class="btn btn-success" style="float: right;display:block" data-toggle="modal" data-target="#addoperation">Add Operation</a>
                            </h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tblseniormanagement" class="DataTable table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Operation Name</th>
                                                <th>Parent</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($operationslookup as $key => $operation) :
                                            ?>
                                                <tr>
                                                    <td><?php echo $key + 1 ?></td>
                                                    <td><?php echo $operation->operationname ?></td>
                                                    <td><?php
                                                        if (array_search($operation->parent, array_column($operationslookup, 'operationid')) === false) {
                                                            echo "-";
                                                        } else {
                                                            echo $operationslookup[array_search($operation->parent, array_column($operationslookup, 'operationid'))]->operationname;
                                                        }
                                                        ?></td>
                                                    <td>
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-trash"></i>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="addoperation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel">Add Operation</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="operationname" class="text-uppercase text-sm">Operation Name</label>
                                                    <input type="text" placeholder="Operation Name" name="operationname" class="form-control mb" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="parent" class="text-uppercase text-sm">Select Parent Operation</label>
                                                    <select name="parent" style="width: 100%;" class="select2 form-control mb">
                                                        <option value="">Select Parent</option>
                                                        <?php

                                                        $sql = "SELECT * from operationslookup WHERE parent = 0";
                                                        $query = $dbh->prepare($sql);
                                                        $query->execute();
                                                        $operationslookup = $query->fetchAll(PDO::FETCH_OBJ);
                                                        $operationslookup_json = json_encode($operationslookup);

                                                        foreach ($operationslookup as $operation) :
                                                        ?>
                                                            <option value="<?php echo $operation->operationid; ?>"><?php echo $operation->operationname; ?></option>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="submit" name="createoperation" class="btn btn-primary">Create Operation</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Loading Scripts -->
        <script>
            window.onload = function() {
                $('.DataTable').DataTable();
            }
        </script>
    </body>

    </html>

<?php endif; ?>