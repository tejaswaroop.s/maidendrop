<?php
session_start();
include('includes/config.php');
include('includes/lookups.php');

if (COUNT($_SESSION) == 0) {
    header('location:index.php');
}

$pageaccess = $_SESSION['rights'][array_search('Invoice', array_column($_SESSION['rights'], 'operationname'))];
if ($pageaccess->_view != 1) {
    header('location:index.php');
} else {

?>
    <!doctype html>
    <html lang="en" class="no-js">

    <head>
        <?php include('includes/header.php'); ?>
    </head>

    <body>
        <div class="ts-main-content">
            <?php include('includes/leftbar.php'); ?>
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Invoices</h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tblinvoices" class="DataTable table table-striped">
                                        <thead>
                                            <tr>
                                                <th>InvoiceId</th>
                                                <th>Name</th>
                                                <th>Branch</th>
                                                <th>Course - Term - Admission Type</th>
                                                <th>Total Invoice Value</th>
                                                <th>Total Paid</th>
                                                <th>Remaining Amount</th>
                                                <th>Download</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if ($_SESSION['userdetails']->branchid == NULL) {
                                                $sql =
                                                    "SELECT InvoiceDetails.TotalValue, InvoiceDetails.invoiceid, InvoiceDetails.name, InvoiceDetails.email, InvoiceDetails.city, InvoiceDetails.campus, InvoiceDetails.admissiontypename, InvoiceDetails.coursename, InvoiceDetails.termname,
                                            PaymentDetails.TotalPaid, (InvoiceDetails.TotalValue - PaymentDetails.TotalPaid) AS RemainingAmount FROM
                                            (SELECT SUM(invoices.feesvalue) as TotalValue, invoices.invoiceid, users.userid, users.name, users.email, branchlookup.city, branchlookup.campus, admissiontypelookup.admissiontypename, courselookup.coursename, termlookup.termname  
                                                                                                         from invoices JOIN users ON invoices.userid = users.userid 
                                                                                                                       JOIN feestructurelookup ON invoices.feesid = feestructurelookup.feesid
                                                                                                                       JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                                                                                       JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                                                                                       JOIN courselookup ON users.courseid = courselookup.courseid
                                                                                                                       JOIN termlookup ON users.termid = termlookup.termid
                                                                                                                       GROUP BY invoices.invoiceid) InvoiceDetails
                                                                                                                       LEFT JOIN
                                                                                                                       (SELECT SUM(payments.paymentamount) as TotalPaid, payments.userid FROM payments JOIN users ON payments.userid = users.userid GROUP BY payments.userid) PaymentDetails ON InvoiceDetails.userid = PaymentDetails.userid";
                                            } else {
                                                $sql =
                                                    "SELECT InvoiceDetails.TotalValue, InvoiceDetails.invoiceid, InvoiceDetails.name, InvoiceDetails.email, InvoiceDetails.city, InvoiceDetails.campus, InvoiceDetails.admissiontypename, InvoiceDetails.coursename, InvoiceDetails.termname,
                                            PaymentDetails.TotalPaid, (InvoiceDetails.TotalValue - PaymentDetails.TotalPaid) AS RemainingAmount FROM
                                            (SELECT SUM(invoices.feesvalue) as TotalValue, invoices.invoiceid, users.userid, users.name, users.email, branchlookup.city, branchlookup.campus, admissiontypelookup.admissiontypename, courselookup.coursename, termlookup.termname  
                                                                                                         from invoices JOIN users ON invoices.userid = users.userid 
                                                                                                                       JOIN feestructurelookup ON invoices.feesid = feestructurelookup.feesid
                                                                                                                       JOIN branchlookup ON users.branchid = branchlookup.branchid
                                                                                                                       JOIN admissiontypelookup ON users.admissiontypeid = admissiontypelookup.admissiontypeid
                                                                                                                       JOIN courselookup ON users.courseid = courselookup.courseid
                                                                                                                       JOIN termlookup ON users.termid = termlookup.termid
                                                                                                                       WHERE users.branchid IN ({$_SESSION['userdetails']->branchid})
                                                                                                                       GROUP BY invoices.invoiceid) InvoiceDetails
                                                                                                                       LEFT JOIN
                                                                                                                       (SELECT SUM(payments.paymentamount) as TotalPaid, payments.userid FROM payments JOIN users ON payments.userid = users.userid GROUP BY payments.userid) PaymentDetails ON InvoiceDetails.userid = PaymentDetails.userid";
                                            }


                                            $query = $dbh->prepare($sql);
                                            $query->execute();
                                            $results = $query->fetchAll(PDO::FETCH_OBJ);

                                            foreach ($results as $result) :
                                            ?>
                                                <tr>
                                                    <td><a target="_blank" href="dompdf/InvoiceDetails.php?invoiceid=<?php echo $result->invoiceid ?>&outputType=View"><?php echo $result->invoiceid ?></a></td>
                                                    <td><?php echo $result->name ?></td>
                                                    <td><?php echo $result->city . ", " . $result->campus ?></td>
                                                    <td><?php echo $result->coursename . ", " . $result->termname . ', ' . $result->admissiontypename ?></td>
                                                    <td><?php echo $result->TotalValue ?></td>
                                                    <td><?php echo $result->TotalPaid == NULL ? "0.00" : $result->TotalPaid ?></td>
                                                    <td><?php echo $result->RemainingAmount == NULL ? $result->TotalValue : $result->RemainingAmount ?></td>
                                                    <td>
                                                        <a title="View" target="_blank" href="dompdf/InvoiceDetails.php?invoiceid=<?php echo $result->invoiceid ?>&outputType=View"><i class="fa fa-eye"></i></a>&nbsp;
                                                        <a title="Download" href="dompdf/InvoiceDetails.php?invoiceid=<?php echo $result->invoiceid ?>&outputType=Download"><i class="fa fa-download"></i></a>&nbsp;
                                                        <a title="Send Email" onclick="sendinvoiceemail('<?php echo $result->invoiceid ?>', '<?php echo $result->email ?>')"><i class="fa fa-paper-plane"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <!-- Loading Scripts -->
        <script>
            window.onload = function() {
                $('.DataTable').DataTable();
                $('.select2').select2();
            }

            function sendinvoiceemail(invoiceid, emailaddress) {
                $.ajax({
                    type: "POST",
                    url: "controller.php",
                    data: {
                        sendinvoiceemail: true,
                        invoiceid: invoiceid,
                        emailaddress: emailaddress
                    },
                    success: () => {
                        alert('Email Sent Successfully');
                    }
                });
            }
        </script>
    </body>

    </html>
<?php } ?>